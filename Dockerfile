# 基础镜像
FROM openjdk:8

#作者
LABEL maintainer=thirdgroup

#复制jar包
COPY *.jar /app.jar

#设定时区
ENV TZ=Asia/Shanghai

#暴露后端端口号
EXPOSE 9090

#入口
ENTRYPOINT ["java","-jar","/app.jar"]