package com.java1234;

import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
/**
 * @author: 1234
 * @date: 2024/12/15 15:09
 * @description: 配置加密密钥测试
 */

@SpringBootTest
@AutoConfigureMockMvc
public class JiamiTest {

    @Autowired
    private StringEncryptor stringEncryptor;
    @Test
    public void test1() {
        String key = "123456";
        System.out.println(key);
    }
    @Test
    void contextLoads() {

    }
    @Test
    public void test(){
//        String keyId = stringEncryptor.encrypt("LTAI5tSC956U7MsULZVM189L");
//        String keySecret = stringEncryptor.encrypt("pIPvSh5EMBKGzIHGnsN2mEtpgyvt4W");
//        String username = stringEncryptor.encrypt("root");
//        String password = stringEncryptor.encrypt("201879Li");
//        String url = stringEncryptor.encrypt("jdbc:mysql://rm-cn-20s3yxrhr000d5xo.rwlb.cn-chengdu.rds.aliyuncs.com:3306/db_admin3?serverTimezone=Asia/Shanghai&useSSL=false");
//        System.out.println("keyId = " + keyId);
//        System.out.println("keySecret = " + keySecret);
//        System.out.println("username = " + username);
//        System.out.println("password = " + password);
//        System.out.println("url = " + url);
//        String bucketName = stringEncryptor.encrypt("sf-works");
//        String endpoint = stringEncryptor.encrypt("https://oss-cn-hangzhou.aliyuncs.com");
//        System.out.println("bucketName = " + bucketName);
//        System.out.println("endpoint = " + endpoint);
//        String jep = stringEncryptor.encrypt("000000");
//        System.out.println("jep = " + jep);
        String key = stringEncryptor.decrypt("SqJwBI0B6QQ4o2dHDm3y3VufKKYYduDI");
        System.out.println("key = " + key);

    }

}
