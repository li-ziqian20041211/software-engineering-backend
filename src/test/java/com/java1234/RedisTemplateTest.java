package com.java1234;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/19 10:58
 */
@SpringBootTest
public class RedisTemplateTest {
    @Autowired
    private RedisTemplate<Object, Object> redisTemplate2;

    @Test
    public void testHash() {
        redisTemplate2.opsForHash().put("user:1", "name", "张三");
        redisTemplate2.opsForHash().put("user:1", "age", "25");
        redisTemplate2.opsForHash().put("user:1", "gender", "男");

        String name = (String) redisTemplate2.opsForHash().get("user:1", "name");
        System.out.println(name);

        //存入MAp
        Map<String, Object> user = new HashMap<>();
        user.put("name", "李四");
        user.put("age", "26");
        redisTemplate2.opsForHash().putAll("user:2", user);

        System.out.println(redisTemplate2.opsForHash().hasKey("user:1", "name"));
    }
}
