package com.java1234.enums;


import lombok.AllArgsConstructor;

/**
 * @author 邓清文
 */

@AllArgsConstructor
public enum ResTypeEnum {
    /**
     * 资源类型枚举
     */

    COA(3, "计算机组成原理"),
    OS(4, "计算机操作系统"),
    CN(2, "计算机网络"),
    DS(1, "数据结构"),
    UNKNOWN(0, "未知");

    private final Integer code;
    private final String desc;


    /**
     * 根据code获取枚举
     */
    public static String getDescByCode(Integer code) {
        for (ResTypeEnum resTypeEnum : ResTypeEnum.values()) {
            if (resTypeEnum.code.equals(code)) {
                return resTypeEnum.desc;
            }
        }
        return ResTypeEnum.UNKNOWN.desc;
    }
    /**
     * 根据desc获取code
     */
    public static Integer getCodeByDesc(String desc) {
        for (ResTypeEnum resTypeEnum : ResTypeEnum.values()) {
            if (resTypeEnum.desc.equals(desc)) {
                return resTypeEnum.code;
            }
        }
        return ResTypeEnum.UNKNOWN.code;
    }

}
