package com.java1234.mapper;

import com.java1234.entity.LoginLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 2675259174
* @description 针对表【login_log(登录日志表)】的数据库操作Mapper
* @createDate 2024-11-19 17:42:31
* @Entity .com.java1234.entity.LoginLog
*/
public interface LoginLogMapper extends BaseMapper<LoginLog> {

}




