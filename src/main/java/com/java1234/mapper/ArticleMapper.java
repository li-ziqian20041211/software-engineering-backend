package com.java1234.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java1234.entity.Article;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ArticleMapper extends BaseMapper<Article> {

    List<Article> selectAll(Article article);
    @Select("SELECT * FROM article WHERE id = #{id}")
    Article selectById(Integer id);

    int insert(Article article);
    int updateById(Article article);



@Delete("DELETE FROM article WHERE id = #{id}")
    void deleteById(Integer id);
}
