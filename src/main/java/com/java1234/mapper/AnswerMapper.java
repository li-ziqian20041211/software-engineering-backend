package com.java1234.mapper;

import com.java1234.entity.Answer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86195
* @description 针对表【answer(答案表)】的数据库操作Mapper
* @createDate 2024-11-22 12:09:41
* @Entity com.java1234.entity.Answer
*/
public interface AnswerMapper extends BaseMapper<Answer> {

}




