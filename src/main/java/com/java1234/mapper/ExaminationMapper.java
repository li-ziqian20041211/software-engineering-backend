package com.java1234.mapper;

import com.java1234.entity.Examination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
* @author 86195
* @description 针对表【examination(试卷表)】的数据库操作Mapper
* @createDate 2024-11-25 00:13:43
* @Entity com.java1234.entity.Examination
*/
public interface ExaminationMapper extends BaseMapper<Examination> {
    @Select("SELECT MAX(id) FROM examination")
    Integer selectMaxId();
}




