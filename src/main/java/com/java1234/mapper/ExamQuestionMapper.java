package com.java1234.mapper;

import com.java1234.entity.ExamQuestion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
* @author 86195
* @description 针对表【exam_question(试卷-试题关联表)】的数据库操作Mapper
* @createDate 2024-11-25 15:06:28
* @Entity com.java1234.entity.ExamQuestion
*/
public interface ExamQuestionMapper extends BaseMapper<ExamQuestion> {
}




