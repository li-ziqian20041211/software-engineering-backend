package com.java1234.mapper;

import com.java1234.entity.PrivateMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86182
* @description 针对表【private_message】的数据库操作Mapper
* @createDate 2024-12-17 16:42:01
* @Entity com.java1234.entity.PrivateMessage
*/
public interface PrivateMessageMapper extends BaseMapper<PrivateMessage> {

}




