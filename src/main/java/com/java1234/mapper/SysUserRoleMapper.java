package com.java1234.mapper;

import com.java1234.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【sys_user_role】的数据库操作Mapper
* @Entity com.java1234.entity.SysUserRole
*/
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}




