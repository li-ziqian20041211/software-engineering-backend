package com.java1234.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java1234.entity.Operationlogs;

/**
* @author 2675259174
* @description 针对表【operationlogs(操作日志表)】的数据库操作Mapper
* @createDate 2024-11-19 13:46:26
* @Entity .com.java1234.entity.Operationlogs
*/
public interface OperationLogsMapper extends BaseMapper<Operationlogs> {

}




