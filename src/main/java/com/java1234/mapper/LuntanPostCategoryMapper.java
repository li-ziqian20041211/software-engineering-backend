package com.java1234.mapper;

import com.java1234.entity.LuntanPostCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86182
* @description 针对表【luntan_post_category】的数据库操作Mapper
* @createDate 2024-11-22 22:34:50
* @Entity com.java1234.entity.LuntanPostCategory
*/
public interface LuntanPostCategoryMapper extends BaseMapper<LuntanPostCategory> {

}




