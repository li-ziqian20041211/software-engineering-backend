package com.java1234.mapper;

import com.java1234.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【sys_menu】的数据库操作Mapper
* @Entity com.java1234.entity.SysMenu
*/
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}




