package com.java1234.mapper;

import com.java1234.entity.QuestionBank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86195
* @description 针对表【question_bank(题库表)】的数据库操作Mapper
* @createDate 2024-11-16 13:14:56
* @Entity com.java1234.entity.QuestionBank
*/
public interface QuestionBankMapper extends BaseMapper<QuestionBank> {

}




