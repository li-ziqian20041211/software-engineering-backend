package com.java1234.mapper;

import com.java1234.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【sys_role_menu】的数据库操作Mapper
* @Entity com.java1234.entity.SysRoleMenu
*/
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}




