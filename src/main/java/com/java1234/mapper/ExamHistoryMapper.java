package com.java1234.mapper;

import com.java1234.entity.ExamHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86195
* @description 针对表【exam_history(练习记录)】的数据库操作Mapper
* @createDate 2024-12-04 15:34:52
* @Entity com.java1234.entity.ExamHistory
*/
public interface ExamHistoryMapper extends BaseMapper<ExamHistory> {

}




