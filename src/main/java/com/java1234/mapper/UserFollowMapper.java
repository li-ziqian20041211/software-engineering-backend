package com.java1234.mapper;

import com.java1234.entity.UserFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86182
* @description 针对表【user_follow(用户关注关系表)】的数据库操作Mapper
* @createDate 2024-12-20 17:21:21
* @Entity com.java1234.entity.UserFollow
*/
public interface UserFollowMapper extends BaseMapper<UserFollow> {

}




