package com.java1234.mapper;

import com.java1234.entity.ExamDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86195
* @description 针对表【exam_detail(答题详情)】的数据库操作Mapper
* @createDate 2024-12-05 14:14:03
* @Entity com.java1234.entity.ExamDetail
*/
public interface ExamDetailMapper extends BaseMapper<ExamDetail> {

}




