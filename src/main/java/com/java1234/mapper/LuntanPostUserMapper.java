package com.java1234.mapper;

import com.java1234.entity.LuntanPostUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86182
* @description 针对表【luntan_post_user】的数据库操作Mapper
* @createDate 2024-11-22 22:35:06
* @Entity com.java1234.entity.LuntanPostUser
*/
public interface LuntanPostUserMapper extends BaseMapper<LuntanPostUser> {

}




