package com.java1234.mapper;

import com.java1234.entity.Luntancategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86182
* @description 针对表【luntancategory(帖子分类)】的数据库操作Mapper
* @createDate 2024-11-13 21:40:16
* @Entity com.java1234.entity.Luntancategory
*/
public interface LuntancategoryMapper extends BaseMapper<Luntancategory> {

}




