package com.java1234.mapper;

import com.java1234.entity.College;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CollegeMapper {

    List<College> selectAll(College college);


    @Delete("DELETE FROM college WHERE id = #{id}")
    void deleteById(Integer id);

    void updateById(College college);

    void insert(College college);
    @Select("SELECT * FROM college WHERE id = #{id}")
    College selectById(Integer id);
}
