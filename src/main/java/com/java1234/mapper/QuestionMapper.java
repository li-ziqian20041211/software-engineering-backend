package com.java1234.mapper;

import com.java1234.entity.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
* @author 86195
* @description 针对表【question(试题表)】的数据库操作Mapper
* @createDate 2024-11-16 13:14:30
* @Entity com.java1234.entity.Question
*/
public interface QuestionMapper extends BaseMapper<Question> {
    @Select("SELECT MAX(id) FROM question")
    Integer selectMaxId();
}




