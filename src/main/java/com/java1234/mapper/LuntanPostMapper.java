package com.java1234.mapper;

import com.java1234.entity.LuntanPost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 86182
* @description 针对表【luntan_post】的数据库操作Mapper
* @createDate 2024-11-19 15:56:39
* @Entity com.java1234.entity.LuntanPost
*/
public interface LuntanPostMapper extends BaseMapper<LuntanPost> {

}




