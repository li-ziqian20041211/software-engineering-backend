package com.java1234.mapper;

import com.java1234.entity.Emp;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDate;
import java.util.List;

/**
 * 资料管理
 * @author 邓清文
 */
@Mapper
public interface EmpMapper {

    /**
     * 资料信息查询
     *
     * @param name    员工姓名
     * @param restype 资源类型
     * @param cont    其他条件
     * @param end     结束日期
     * @return 符合条件的员工列表
     */
    List<Emp> list(String name, Short restype, Integer cont, LocalDate end);

    /**
     * 批量删除
     *
     * @param ids 要删除的员工ID列表
     */
    void delete(List<Integer> ids);

    /**
     * 新增
     *
     * @param emp 要新增的员工对象
     */
    @Insert("insert into emp(name, restype, image, cont, create_time, update_time) " +
            "values(#{name}, #{restype}, #{image},  #{cont}, #{createTime}, #{updateTime})")
    void insert(Emp emp);

    /**
     * 根据ID查询
     *
     * @param id 员工ID
     * @return 对应ID的员工对象
     */
    @Select("select * from emp where id = #{id}")
    Emp getById(Integer id);

    /**
     * 更新
     *
     * @param emp 要更新的员工对象
     */
    void update(Emp emp);

    /**
     * 更新下载次数
     *
     * @param emp 包含ID的员工对象，用于更新其下载次数
     */
    @Update("update emp set cont = cont + 1 where id = #{id}")
    void contUpdate(Emp emp);

    /**
     * 查询所有资料
     *
     * @return 所有员工的列表
     */
    @Select("select * from emp")
    List<Emp> selectList();


    @Select("SELECT COUNT(*) > 0 FROM emp WHERE name = #{name} AND image = #{image}")
    boolean existsByNameAndImage(String name, String image);

}
