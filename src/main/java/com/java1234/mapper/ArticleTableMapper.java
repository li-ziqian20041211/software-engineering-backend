package com.java1234.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java1234.entity.ArticleTable;

public interface ArticleTableMapper extends BaseMapper<ArticleTable> {
}
