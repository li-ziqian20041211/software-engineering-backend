package com.java1234.mapper;

import com.java1234.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @description 针对表【sys_user】的数据库操作Mapper
* @Entity com.java1234.entity.SysUser
*/
public interface SysUserMapper extends BaseMapper<SysUser> {

}




