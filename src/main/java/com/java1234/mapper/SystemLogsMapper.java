package com.java1234.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java1234.entity.SystemLogs;

/**
* @author 2675259174
* @description 针对表【system_logs(系统日志表)】的数据库操作Mapper
* @createDate 2024-11-19 19:28:39
* @Entity .com.java1234.entity.SystemLogs
*/
public interface SystemLogsMapper extends BaseMapper<SystemLogs> {
}



