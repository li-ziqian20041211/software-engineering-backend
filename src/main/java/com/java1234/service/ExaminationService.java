package com.java1234.service;

import com.java1234.entity.Examination;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86195
* @description 针对表【examination(试卷表)】的数据库操作Service
* @createDate 2024-11-25 00:13:43
*/
public interface ExaminationService extends IService<Examination> {
    Integer getMaxExaminationId();
}
