package com.java1234.service;

import com.java1234.entity.Operationlogs;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 2675259174
* @description 针对表【operationlogs(操作日志表)】的数据库操作Service
* @createDate 2024-11-19 13:46:26
*/
public interface OperationLogsService extends IService<Operationlogs> {
    void logOperation(Operationlogs log);
    List<Operationlogs> getAllLogs();
}
