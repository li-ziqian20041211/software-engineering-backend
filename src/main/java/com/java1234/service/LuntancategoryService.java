package com.java1234.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java1234.entity.Luntancategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 86182
* @description 针对表【luntancategory(帖子分类)】的数据库操作Service
* @createDate 2024-11-13 21:40:16
*/
public interface LuntancategoryService extends IService<Luntancategory> {

    // 分页查询帖子分类列表
    // 定义分页查询帖子分类列表的方法，接收分页对象和查询条件包装器作为参数
    Page<Luntancategory> getCategoryPage(Page<Luntancategory> page, QueryWrapper<Luntancategory> queryWrapper);



    // 根据ID查询帖子分类详情
    Luntancategory getCategoryById(Integer id);

    // 新增帖子分类
    boolean addCategory(Luntancategory category);

    // 根据ID删除帖子分类
    boolean deleteCategoryById(Integer id);

    // 批量删除帖子分类
    boolean deleteCategoriesByIds(List<Integer> ids);

    // 更新帖子分类信息
    boolean updateCategory(Luntancategory category);

    List<Luntancategory> getAllCategory();
}
