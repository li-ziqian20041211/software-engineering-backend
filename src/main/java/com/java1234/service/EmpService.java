package com.java1234.service;

import com.java1234.entity.Emp;
import com.java1234.entity.PageBean;


import java.time.LocalDate;
import java.util.List;

/**
 * EmpService接口提供了一系列用于管理资料（原员工）实体的操作方法。
 * @author 邓清文
 */
public interface EmpService {

    /**
     * 分页查询资料信息。
     *
     * @param page 页码，从1开始。
     * @param pageSize 每页显示的记录数。
     * @param name 资料姓名或部分姓名，用于模糊搜索。
     * @param restype 休息类型代码，可以用来过滤结果。
     * @param cont 其他条件，具体含义需根据业务逻辑定义。
     * @param begin 开始日期，查询在此日期之后（包括）的记录。
     * @param end 结束日期，查询在此日期之前（包括）的记录。
     * @return 包含分页数据和附加信息的ResPageBean对象。
     */
    PageBean page(Integer page, Integer pageSize, String name, Short restype, Integer cont, LocalDate begin, LocalDate end);

    /**
     * 根据提供的ID列表批量删除资料记录。
     *
     * @param ids 要删除的资料ID列表。
     */
    void delete(List<Integer> ids);

    /**
     * 新增资料记录。
     *
     * @param emp 包含新资料信息的Emp对象。
     */
    Boolean save(Emp emp);

    /**
     * 根据ID查询单个资料的详细信息。
     *
     * @param id 资料ID。
     * @return 包含指定ID资料信息的Emp对象，若无该资料则返回null。
     */
    Emp getById(Integer id);

    /**
     * 更新现有资料的信息。
     *
     * @param emp 包含更新后信息的Emp对象。
     */
    void update(Emp emp);

    /**
     * 更新与计数相关的资料属性。
     * 此方法可能是为了更新某些特定的统计字段，如工作时间、假期天数等。
     *
     * @param emp 包含需要更新的计数信息的Emp对象。
     */
    void contUpdate(Emp emp);

    /**
     * 获取所有资料的列表。
     *
     * @return 包含所有资料信息的Emp对象列表。
     */
    List<Emp> getList();

    boolean existsByNameAndImage(String name, String image);
}