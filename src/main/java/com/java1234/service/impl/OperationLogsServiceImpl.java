package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.Operationlogs;
import com.java1234.service.OperationLogsService;
import com.java1234.mapper.OperationLogsMapper;
import org.springframework.stereotype.Service;

import java.util.List;


/**
* @author 2675259174
* @description 针对表【operationlogs(操作日志表)】的数据库操作Service实现
* @createDate 2024-11-19 13:46:26
*/
@Service
public class OperationLogsServiceImpl extends ServiceImpl<OperationLogsMapper, Operationlogs> implements OperationLogsService {

    @Override
    public void logOperation(Operationlogs log) {
        save(log); // 使用 MyBatis-Plus 的 save 方法保存日志
    }

    @Override
    public List<Operationlogs> getAllLogs() {
        return list(); // 使用 MyBatis-Plus 的 list 方法获取所有日志
    }
}




