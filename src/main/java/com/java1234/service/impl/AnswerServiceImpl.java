package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.Answer;
import com.java1234.service.AnswerService;
import com.java1234.mapper.AnswerMapper;
import org.springframework.stereotype.Service;

/**
* @author 86195
* @description 针对表【answer(答案表)】的数据库操作Service实现
* @createDate 2024-11-22 12:09:41
*/
@Service
public class AnswerServiceImpl extends ServiceImpl<AnswerMapper, Answer>
    implements AnswerService{

}




