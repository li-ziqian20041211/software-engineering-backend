package com.java1234.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.UserFollow;
import com.java1234.service.SysUserService;
import com.java1234.service.UserFollowService;
import com.java1234.mapper.UserFollowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
* @author 86182
* @description 针对表【user_follow(用户关注关系表)】的数据库操作Service实现
* @createDate 2024-12-20 17:21:21
*/
@Service
public class UserFollowServiceImpl extends ServiceImpl<UserFollowMapper, UserFollow>
    implements UserFollowService{
    @Autowired
    private SysUserService sysUserService;

    /**
     * 关注用户
     */
    @Override
    public void followUser(Long followerId, Long followingId) {
        // 检查是否已经关注
        QueryWrapper<UserFollow> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("follower_id", followerId).eq("following_id", followingId);
        if (this.count(queryWrapper) > 0) {
            throw new RuntimeException("已经关注该用户");
        }

        // 插入关注记录
        UserFollow userFollow = new UserFollow();
        userFollow.setFollowerId(followerId);
        userFollow.setFollowingId(followingId);
        userFollow.setCreateTime(new Date());
        this.save(userFollow);
    }

    /**
     * 取消关注用户
     */
    @Override
    public void unfollowUser(Long followerId, Long followingId) {
        QueryWrapper<UserFollow> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("follower_id", followerId).eq("following_id", followingId);
        this.remove(queryWrapper);
    }

    /**
     * 检查是否已关注
     */
    @Override
    public boolean isFollowing(Long followerId, Long followingId) {
        QueryWrapper<UserFollow> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("follower_id", followerId).eq("following_id", followingId);
        return this.count(queryWrapper) > 0;
    }

    /**
     * 获取用户的关注数量
     */
    @Override
    public int getFollowingCount(Long userId) {
        QueryWrapper<UserFollow> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("follower_id", userId); // 查询 follower_id 为 userId 的记录
        return this.count(queryWrapper);
    }

    /**
     * 获取用户的粉丝数量
     */
    @Override
    public int getFollowerCount(Long userId) {
        QueryWrapper<UserFollow> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("following_id", userId); // 查询 following_id 为 userId 的记录
        return this.count(queryWrapper);
    }

}




