package com.java1234.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.ExamQuestion;
import com.java1234.service.ExamQuestionService;
import com.java1234.mapper.ExamQuestionMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 86195
* @description 针对表【exam_question(试卷-试题关联表)】的数据库操作Service实现
* @createDate 2024-11-25 15:06:28
*/
@Service
public class ExamQuestionServiceImpl extends ServiceImpl<ExamQuestionMapper, ExamQuestion>
    implements ExamQuestionService{
    @Override
    public boolean saveBatch(List<ExamQuestion> examQuestions) {
        return super.saveBatch(examQuestions);
    }

    @Override
    public void removeByExaminationIds(List<Long> examinationIds) {
        this.remove(new QueryWrapper<ExamQuestion>().in("examination_id", examinationIds));
    }

    @Override
    public List<ExamQuestion> listByExaminationId(Long examinationId) {
        return this.list(new QueryWrapper<ExamQuestion>().eq("examination_id", examinationId));
    }

    @Override
    public boolean removeByExaminationId(Long examinationId) {
        QueryWrapper<ExamQuestion> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("examination_id", examinationId);
        return remove(queryWrapper);
    }
}




