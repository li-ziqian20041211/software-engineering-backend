package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.Question;
import com.java1234.service.QuestionService;
import com.java1234.mapper.QuestionMapper;
import org.springframework.stereotype.Service;

/**
* @author 86195
* @description 针对表【question(试题表)】的数据库操作Service实现
* @createDate 2024-11-16 13:14:30
*/
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question>
    implements QuestionService{
    @Override
    public Integer getMaxQuestionId() {
        return baseMapper.selectMaxId();
    }

}




