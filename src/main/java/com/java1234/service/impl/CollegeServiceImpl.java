package com.java1234.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.java1234.entity.College;

import com.java1234.mapper.CollegeMapper;
import com.java1234.service.CollegeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CollegeServiceImpl implements CollegeService {
    @Resource
    private CollegeMapper collegeMapper;
    @Override
    public void add(College college) {

        collegeMapper.insert(college);
    }
    @Override
    public List<College> selectAll(College college) {
        return collegeMapper.selectAll(college);

    }

    @Override
    public College selectById(Integer id) {
        return collegeMapper.selectById(id);
    }

    @Override
    public PageInfo<College> selectPage(College college, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<College> list=collegeMapper.selectAll(college);
        return PageInfo.of(list);
    }

    @Override
    public void update(College college) {
        collegeMapper.updateById(college);
    }
    @Override
    public void deleteById(Integer id) {
        collegeMapper.deleteById(id);
    }
    @Override
    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids){
            collegeMapper.deleteById(id);
        }
    }
}
