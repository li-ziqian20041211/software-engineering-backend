package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.LuntanPostUser;
import com.java1234.service.LuntanPostUserService;
import com.java1234.mapper.LuntanPostUserMapper;
import org.springframework.stereotype.Service;

/**
* @author 86182
* @description 针对表【luntan_post_user】的数据库操作Service实现
* @createDate 2024-11-22 22:35:06
*/
@Service
public class LuntanPostUserServiceImpl extends ServiceImpl<LuntanPostUserMapper, LuntanPostUser>
    implements LuntanPostUserService{

}




