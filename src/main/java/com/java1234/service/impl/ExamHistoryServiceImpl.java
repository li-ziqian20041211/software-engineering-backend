package com.java1234.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.ExamHistory;
import com.java1234.service.ExamHistoryService;
import com.java1234.mapper.ExamHistoryMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 86195
* @description 针对表【exam_history(练习记录)】的数据库操作Service实现
* @createDate 2024-12-04 15:34:52
*/
@Service
public class ExamHistoryServiceImpl extends ServiceImpl<ExamHistoryMapper, ExamHistory>
    implements ExamHistoryService{
    @Override
    public List<ExamHistory> listByUserId(Long userId) {
        return this.list(new QueryWrapper<ExamHistory>().eq("user_id", userId));
    }
    @Override
    public void saveExamHistory(ExamHistory examHistory) {
        this.save(examHistory);
    }
    @Override
    public IPage<ExamHistory> listByUserId(Long userId, Page<ExamHistory> page) {
        return this.page(page, new QueryWrapper<ExamHistory>().eq("user_id", userId));
    }
}




