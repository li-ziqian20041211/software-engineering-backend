package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.LuntanPostCategory;
import com.java1234.service.LuntanPostCategoryService;
import com.java1234.mapper.LuntanPostCategoryMapper;
import org.springframework.stereotype.Service;

/**
* @author 86182
* @description 针对表【luntan_post_category】的数据库操作Service实现
* @createDate 2024-11-22 22:34:50
*/
@Service
public class LuntanPostCategoryServiceImpl extends ServiceImpl<LuntanPostCategoryMapper, LuntanPostCategory>
    implements LuntanPostCategoryService{

}




