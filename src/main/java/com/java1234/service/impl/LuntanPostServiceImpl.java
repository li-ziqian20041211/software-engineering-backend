package com.java1234.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.LuntanPost;
import com.java1234.entity.LuntanPostCategory;
import com.java1234.service.LuntanPostCategoryService;
import com.java1234.service.LuntanPostService;
import com.java1234.mapper.LuntanPostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
* @author 86182
* @description 针对表【luntan_post】的数据库操作Service实现
* @createDate 2024-11-19 15:56:39
*/
@Service
public class LuntanPostServiceImpl extends ServiceImpl<LuntanPostMapper, LuntanPost>
    implements LuntanPostService{

    @Autowired
    private LuntanPostMapper luntanPostMapper;

    @Autowired
    private LuntanPostCategoryService luntanPostCategoryService;


    @Override
    public boolean deletePostById(Integer id) {
        return removeById(id);
    }

    @Override
    public boolean updatePost(LuntanPost post) {
        // 更新帖子基本信息
        boolean updated = updateById(post);
        if (!updated) {
            return false;
        }

        // 删除旧的分类关联
        QueryWrapper<LuntanPostCategory> deleteWrapper = new QueryWrapper<>();
        deleteWrapper.eq("post_id", post.getId());
        luntanPostCategoryService.remove(deleteWrapper);

        // 插入新的分类关联
        if (post.getCategoryIds() != null && !post.getCategoryIds().isEmpty()) {
            List<LuntanPostCategory> postCategories = post.getCategoryIds().stream()
                    .map(categoryId -> new LuntanPostCategory(post.getId(), categoryId))
                    .collect(Collectors.toList());
            luntanPostCategoryService.saveBatch(postCategories);
        }

        return true;
    }

    @Override
    public LuntanPost getPostById(Integer id) {
        return luntanPostMapper.selectById(id);
    }

    @Override
    public void insertCommentOrReply(LuntanPost commentOrReply) {
        // 插入评论或回复
        luntanPostMapper.insert(commentOrReply);

        // 获取目标帖子的当前评论数量
        LuntanPost targetPost;
        if (Boolean.TRUE.equals(commentOrReply.getIsComment()) && Boolean.FALSE.equals(commentOrReply.getIsReply())) {
            // 如果是评论，直接获取目标帖子
            targetPost = luntanPostMapper.selectById(commentOrReply.getParentId());
        } else if (Boolean.FALSE.equals(commentOrReply.getIsComment()) && Boolean.TRUE.equals(commentOrReply.getIsReply())) {
            // 如果是回复，先获取评论，再获取评论的父帖子
            LuntanPost comment = luntanPostMapper.selectById(commentOrReply.getParentId());
            targetPost = luntanPostMapper.selectById(comment.getParentId());
        } else {
            // 如果既不是评论也不是回复，直接返回
            return;
        }

        int currentComments = targetPost.getComments();

        // 更新目标帖子的评论数量
        targetPost.setComments(currentComments + 1);
        luntanPostMapper.updateById(targetPost);
    }

    @Override
    public void deleteCommentOrReply(LuntanPost commentOrReply) {
        // 获取目标帖子的当前评论数量
        LuntanPost targetPost;
        if (Boolean.TRUE.equals(commentOrReply.getIsComment()) && Boolean.FALSE.equals(commentOrReply.getIsReply())) {
            // 如果是评论，直接获取目标帖子
            targetPost = luntanPostMapper.selectById(commentOrReply.getParentId());
        } else if (Boolean.FALSE.equals(commentOrReply.getIsComment()) && Boolean.TRUE.equals(commentOrReply.getIsReply())) {
            // 如果是回复，先获取评论，再获取评论的父帖子
            LuntanPost comment = luntanPostMapper.selectById(commentOrReply.getParentId());
            targetPost = luntanPostMapper.selectById(comment.getParentId());
        } else {
            // 如果既不是评论也不是回复，直接返回
            return;
        }

        int currentComments = targetPost.getComments();

        // 删除评论或回复
        luntanPostMapper.deleteById(commentOrReply.getId());

        // 更新目标帖子的评论数量
        targetPost.setComments(currentComments - 1);
        luntanPostMapper.updateById(targetPost);
    }

    @Override
    public void incrementLikes(Integer postId) {
        LuntanPost post = luntanPostMapper.selectById(postId);
        if (post != null) {
            post.setLikes(post.getLikes() + 1);
            luntanPostMapper.updateById(post);
        }
    }

    @Override
    public void incrementViews(Integer postId) {
        LuntanPost post = luntanPostMapper.selectById(postId);
        if (post != null) {
            post.setViews(post.getViews() + 1);
            luntanPostMapper.updateById(post);
        }
    }
}




