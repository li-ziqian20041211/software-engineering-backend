package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.LoginLog;
import com.java1234.mapper.LoginLogMapper;
import com.java1234.service.LoginLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 2675259174
* @description 针对表【login_log(登录日志表)】的数据库操作Service实现
* @createDate 2024-11-19 17:42:31
*/
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLog> implements LoginLogService {

    @Override
    public void logLogin(LoginLog loginLog) {save(loginLog); }// 使用 MyBatis-Plus 的 save 方法保存日志

    @Override
    public List<LoginLog> getAllLoginLogs() { return list();}// 使用 MyBatis-Plus 的 list 方法获取所有日志
}



