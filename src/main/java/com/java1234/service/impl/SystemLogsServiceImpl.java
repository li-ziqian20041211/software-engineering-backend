package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.SystemLogs;
import com.java1234.mapper.SystemLogsMapper;
import com.java1234.service.SystemLogsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 2675259174
 * @description 针对表【system_logs(系统日志表)】的数据库操作Service实现
 * @createDate 2024-11-19 19:28:39
 */
@Service
public class SystemLogsServiceImpl extends ServiceImpl<SystemLogsMapper, SystemLogs> implements SystemLogsService {

    @Override
    public void SystemLog(SystemLogs systemLogs) {save(systemLogs); }// 使用 MyBatis-Plus 的 save 方法保存日志

    @Override
    public List<SystemLogs> getAllSystemLogs() { return list();}// 使用 MyBatis-Plus 的 list 方法获取所有日志

    @Override
    public void logSystem(SystemLogs log) {

    }
}