package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.Examination;
import com.java1234.service.ExaminationService;
import com.java1234.mapper.ExaminationMapper;
import org.springframework.stereotype.Service;

/**
* @author 86195
* @description 针对表【examination(试卷表)】的数据库操作Service实现
* @createDate 2024-11-25 00:13:43
*/
@Service
public class ExaminationServiceImpl extends ServiceImpl<ExaminationMapper, Examination>
    implements ExaminationService{
    @Override
    public Integer getMaxExaminationId() {
        return baseMapper.selectMaxId();
    }
}




