package com.java1234.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.PrivateMessage;
import com.java1234.entity.SysUser;
import com.java1234.service.PrivateMessageService;
import com.java1234.mapper.PrivateMessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
* @author 86182
* @description 针对表【private_message】的数据库操作Service实现
* @createDate 2024-12-17 16:42:01
*/
@Service
public class PrivateMessageServiceImpl extends ServiceImpl<PrivateMessageMapper, PrivateMessage>
        implements PrivateMessageService {
    @Autowired
    private PrivateMessageMapper privateMessageMapper;

    @Override
    public void sendPrivateMessage(SysUser sender, SysUser receiver, String content) {
        PrivateMessage message = new PrivateMessage();
        message.setSenderId(sender.getId());
        message.setReceiverId(receiver.getId());
        message.setContent(content);
        message.setSentAt(LocalDateTime.now());
        privateMessageMapper.insert(message);
    }

    @Override
    public List<PrivateMessage> getPrivateMessagesByUsers(Long senderId, Long receiverId) {
        return privateMessageMapper.selectList(
                new QueryWrapper<PrivateMessage>()
                        .and(wrapper -> wrapper.eq("sender_id", senderId).eq("receiver_id", receiverId))
                        .or(wrapper -> wrapper.eq("sender_id", receiverId).eq("receiver_id", senderId))
                        .orderByAsc("sent_at")
        );
    }
}




