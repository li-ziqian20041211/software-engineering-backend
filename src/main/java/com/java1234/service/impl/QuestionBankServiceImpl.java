package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.QuestionBank;
import com.java1234.service.QuestionBankService;
import com.java1234.mapper.QuestionBankMapper;
import org.springframework.stereotype.Service;

/**
* @author 86195
* @description 针对表【question_bank(题库表)】的数据库操作Service实现
* @createDate 2024-11-16 13:14:56
*/
@Service
public class QuestionBankServiceImpl extends ServiceImpl<QuestionBankMapper, QuestionBank>
    implements QuestionBankService{

}




