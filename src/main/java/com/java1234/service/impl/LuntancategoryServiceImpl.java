package com.java1234.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.Luntancategory;
import com.java1234.service.LuntancategoryService;
import com.java1234.mapper.LuntancategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
/**
* @author 86182
* @description 针对表【luntancategory(帖子分类)】的数据库操作Service实现
* @createDate 2024-11-13 21:40:16
*/
@Service
public class LuntancategoryServiceImpl extends ServiceImpl<LuntancategoryMapper, Luntancategory>
    implements LuntancategoryService{

    @Resource
    private LuntancategoryMapper baseMapper;
    @Autowired
    private LuntancategoryMapper luntancategoryMapper;


    @Override
    public Page<Luntancategory> getCategoryPage(Page<Luntancategory> page, QueryWrapper<Luntancategory> queryWrapper) {
        // 通过MyBatis-Plus的BaseMapper接口的selectPage方法执行分页查询，传入分页对象和查询条件包装器
        return baseMapper.selectPage(page, queryWrapper);
    }

    @Override
    public Luntancategory getCategoryById(Integer id) {
        return baseMapper.selectById(id);
    }

    @Override
    public boolean addCategory(Luntancategory category) {
        category.setCreateTime(new java.util.Date()); // 假设帖子分类有创建时间字段，设置为当前时间
        return save(category);
    }

    @Override
    public boolean deleteCategoryById(Integer id) {
        return removeById(id);
    }

    @Override
    public boolean deleteCategoriesByIds(List<Integer> ids) {
        return removeByIds(ids);
    }

    @Override
    public boolean updateCategory(Luntancategory category) {
        return updateById(category);
    }

    // 新增的查询所有分类的方法
    @Override
    public List<Luntancategory> getAllCategory() {
        // 创建一个空的查询条件包装器，即不添加任何额外的筛选条件，表示查询所有记录
        QueryWrapper<Luntancategory> queryWrapper = new QueryWrapper<>();
        // 调用BaseMapper的selectList方法，传入查询条件包装器，获取所有分类数据
        return baseMapper.selectList(queryWrapper);
    }



}




