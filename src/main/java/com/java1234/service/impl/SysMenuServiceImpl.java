package com.java1234.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.java1234.entity.SysMenu;
import com.java1234.service.SysMenuService;
import com.java1234.mapper.SysMenuMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @description 针对表【sys_menu】的数据库操作Service实现
*/
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
    implements SysMenuService{

    @Override
    public List<SysMenu> buildTreeMenu(List<SysMenu> sysMenuList) {
        List<SysMenu> resultMenuList=new ArrayList<>();

        for(SysMenu sysMenu:sysMenuList){

            // 寻找子节点
            for(SysMenu e:sysMenuList){
                if(e.getParentId()==sysMenu.getId()){
                    sysMenu.getChildren().add(e);
                }
            }

            if(sysMenu.getParentId()==0L){
                resultMenuList.add(sysMenu);
            }
        }

        return resultMenuList;
    }
}






//package com.java1234.service.impl;
//
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import com.java1234.entity.SysMenu;
//import com.java1234.service.SysMenuService;
//import com.java1234.mapper.SysMenuMapper;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @description 针对表【sys_menu】的数据库操作Service实现
// */
//@Service
//public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
//        implements SysMenuService{
//
//    @Override
//    public List<SysMenu> buildTreeMenu(List<SysMenu> sysMenuList) {
//        List<SysMenu> resultMenuList = new ArrayList<>();
//
//        for (SysMenu sysMenu : sysMenuList) {
//            if (sysMenu.getParentId() == 0L) {
//                resultMenuList.add(sysMenu);
//            }
//        }
//
//        for (SysMenu sysMenu : resultMenuList) {
//            setChildren(sysMenu, sysMenuList);
//        }
//
//        return resultMenuList;
//    }
//
//    private void setChildren(SysMenu parentMenu, List<SysMenu> sysMenuList) {
//        for (SysMenu sysMenu : sysMenuList) {
//            if (sysMenu.getParentId().equals(parentMenu.getId())) {
//                parentMenu.getChildren().add(sysMenu);
//                setChildren(sysMenu, sysMenuList);
//            }
//        }
//    }
//}