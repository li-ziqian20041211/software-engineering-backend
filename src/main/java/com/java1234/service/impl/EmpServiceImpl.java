package com.java1234.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.java1234.entity.PageBean;
import com.java1234.mapper.EmpMapper;
import com.java1234.entity.Emp;
import com.java1234.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.time.LocalDate;

import java.util.List;
/**
 * @author 邓清文
 */
@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpMapper empMapper;

    @Override
    public PageBean page(Integer page, Integer pageSize, String name, Short restype, Integer cont, LocalDate begin, LocalDate end) {
        //1. 设置分页参数
        PageHelper.startPage(page,pageSize);

        //2. 执行查询
        List<Emp> empList = empMapper.list(name, restype, cont, end);
        Page<Emp> p = (Page<Emp>) empList;

        return new PageBean(p.getTotal(), p.getResult());
    }

    @Override
    public void delete(List<Integer> ids) {
        empMapper.delete(ids);
    }

    @Override
    public Boolean save(Emp emp) {
        Date now = new Date();
        emp.setCreateTime(now);
        emp.setUpdateTime(now);
        emp.setCont(0);
        empMapper.insert(emp);
        return true;
    }
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Emp getById(Integer id) {
        return empMapper.getById(id);
    }

    @Override
    public void update(Emp emp) {
        Date now = new Date();
        emp.setUpdateTime(now);
        empMapper.update(emp);
    }

    @Override
    public void contUpdate(Emp emp) {
        empMapper.contUpdate(emp);
    }

    @Override
    public List<Emp> getList() {
        return empMapper.selectList();
    }

    @Override
    public boolean existsByNameAndImage(String name, String image) {
        return empMapper.existsByNameAndImage(name, image);
    }




}
