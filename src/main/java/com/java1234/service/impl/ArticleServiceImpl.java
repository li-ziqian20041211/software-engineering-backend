package com.java1234.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.java1234.entity.Article;
import com.java1234.entity.PageResult;
import com.java1234.entity.RequestParams;
import com.java1234.mapper.ArticleMapper;

import com.java1234.service.ArticleService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl   extends ServiceImpl<ArticleMapper, Article> implements ArticleService {

    @Resource
    private ArticleMapper articleMapper;
    @Autowired
    private RestHighLevelClient restHighLevelClient;


    @Override
    public Article add(Article article) {
        // 保存文章到数据库
        articleMapper.insert(article);
        return article;
    }
    @Override
    public List<Article> selectAll(Article article) {
         return articleMapper.selectAll(article);

    }

    @Override
    public Article selectById(Integer id) {
        return articleMapper.selectById(id);
    }




    @Override
    public PageInfo<Article> selectPage(Article article, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Article> list=articleMapper.selectAll(article);
        return PageInfo.of(list);
    }

    @Override
    public Article update(Article article) {
        articleMapper.updateById(article);
        return article;
    }
    @Override
    public void deleteById(Integer id) {
        articleMapper.deleteById(id);
    }
    @Override
    public void deleteBatch(List<Integer> ids) {
        for (Integer id : ids){
            articleMapper.deleteById(id);
        }
    }


}
