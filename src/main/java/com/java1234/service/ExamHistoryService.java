package com.java1234.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java1234.entity.ExamHistory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 86195
* @description 针对表【exam_history(练习记录)】的数据库操作Service
* @createDate 2024-12-04 15:34:52
*/
public interface ExamHistoryService extends IService<ExamHistory> {
    List<ExamHistory> listByUserId(Long userId);
    void saveExamHistory(ExamHistory examHistory);
    IPage<ExamHistory> listByUserId(Long userId, Page<ExamHistory> page);
}
