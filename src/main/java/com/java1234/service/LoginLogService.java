package com.java1234.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java1234.entity.LoginLog;

import java.util.List;

/**
* @author 2675259174
* @description 针对表【login_log(登录日志表)】的数据库操作Service
* @createDate 2024-11-19 17:42:31
*/
public interface LoginLogService extends IService<LoginLog> {

    List<LoginLog> getAllLoginLogs();

    void logLogin(LoginLog log);
}
