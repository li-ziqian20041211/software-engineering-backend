package com.java1234.service;

import com.java1234.entity.QuestionBank;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86195
* @description 针对表【question_bank(题库表)】的数据库操作Service
* @createDate 2024-11-16 13:14:56
*/
public interface QuestionBankService extends IService<QuestionBank> {

}
