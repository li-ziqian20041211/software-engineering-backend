package com.java1234.service;

import com.github.pagehelper.PageInfo;
import com.java1234.entity.College;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CollegeService {
    void deleteById(Integer id);

    void deleteBatch(List<Integer> ids);

    void update(College college);

    void add(College college);


    College selectById(Integer id);

    PageInfo<College> selectPage(College college, Integer pageNum, Integer pageSize);


    List<College> selectAll(College college);

}
