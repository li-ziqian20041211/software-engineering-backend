package com.java1234.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java1234.entity.LuntanPost;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java1234.entity.Luntancategory;


import java.util.List;
/**
* @author 86182
* @description 针对表【luntan_post】的数据库操作Service
* @createDate 2024-11-19 15:56:39
*/
public interface LuntanPostService extends IService<LuntanPost> {
    boolean deletePostById(Integer id);

    // 更新帖子分类信息
    boolean updatePost(LuntanPost post);
    // 根据ID查询帖子分类详情
    LuntanPost getPostById(Integer id);


    void insertCommentOrReply(LuntanPost commentOrReply);

    void deleteCommentOrReply(LuntanPost commentOrReply);

    void incrementLikes(Integer postId);

    void incrementViews(Integer postId);
}
