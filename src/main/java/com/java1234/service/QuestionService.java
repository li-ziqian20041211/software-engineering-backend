package com.java1234.service;

import com.java1234.entity.Question;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86195
* @description 针对表【question(试题表)】的数据库操作Service
* @createDate 2024-11-16 13:14:30
*/
public interface QuestionService extends IService<Question> {
    Integer getMaxQuestionId();
}
