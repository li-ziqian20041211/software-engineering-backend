package com.java1234.service;

import com.java1234.entity.UserFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86182
* @description 针对表【user_follow(用户关注关系表)】的数据库操作Service
* @createDate 2024-12-20 17:21:21
*/
public interface UserFollowService extends IService<UserFollow> {

    void followUser(Long followerId, Long followingId);

    void unfollowUser(Long followerId, Long followingId);

    boolean isFollowing(Long followerId, Long followingId);

    int getFollowingCount(Long userId);

    int getFollowerCount(Long userId);
}
