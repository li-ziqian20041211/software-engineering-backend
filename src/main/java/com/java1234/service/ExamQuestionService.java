package com.java1234.service;

import com.java1234.entity.ExamQuestion;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 86195
* @description 针对表【exam_question(试卷-试题关联表)】的数据库操作Service
* @createDate 2024-11-25 15:06:28
*/
public interface ExamQuestionService extends IService<ExamQuestion> {
    boolean saveBatch(List<ExamQuestion> examQuestions);
    void removeByExaminationIds(List<Long> examinationIds);
    List<ExamQuestion> listByExaminationId(Long examinationId);
    boolean removeByExaminationId(Long examinationId);
}
