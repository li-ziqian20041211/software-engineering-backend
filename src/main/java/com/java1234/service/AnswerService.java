package com.java1234.service;

import com.java1234.entity.Answer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86195
* @description 针对表【answer(答案表)】的数据库操作Service
* @createDate 2024-11-22 12:09:41
*/
public interface AnswerService extends IService<Answer> {

}
