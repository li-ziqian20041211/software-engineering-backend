package com.java1234.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.java1234.entity.Article;
import com.java1234.entity.PageResult;
import com.java1234.entity.RequestParams;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 张若瑜
 */
@Service
public interface ArticleService extends IService<Article> {
    void deleteById(Integer id);
    void deleteBatch(List<Integer> ids);

    List<Article> selectAll(Article article);

    PageInfo<Article> selectPage(Article article, Integer pageNum, Integer pageSize);

    Article update(Article article);

    Article add(Article article);

    Article selectById(Integer id);


}
