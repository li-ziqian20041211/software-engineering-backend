package com.java1234.service;

import com.java1234.entity.PrivateMessage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java1234.entity.SysUser;

import java.util.List;

/**
* @author 86182
* @description 针对表【private_message】的数据库操作Service
* @createDate 2024-12-17 16:42:01
*/
public interface PrivateMessageService extends IService<PrivateMessage> {
    void sendPrivateMessage(SysUser sender, SysUser receiver, String content);

    List<PrivateMessage> getPrivateMessagesByUsers(Long senderId, Long receiverId);
}
