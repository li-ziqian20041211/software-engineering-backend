package com.java1234.service;



import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.java1234.entity.Article;
import com.java1234.entity.ArticleTable;
import com.java1234.entity.PageResult;
import com.java1234.entity.RequestParams;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 张若瑜
 */
@Service
public interface ArticleTableService extends IService<ArticleTable> {

    PageResult search(RequestParams params);
}
