package com.java1234.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java1234.entity.SystemLogs;

import java.util.List;

/**
 * @author 2675259174
 * @description 针对表【system_logs(系统日志表)】的数据库操作Service
 * @createDate 2024-11-19 19:28:39
 */
public interface SystemLogsService extends IService<SystemLogs> {

    void SystemLog(SystemLogs systemLogs)// 使用 MyBatis-Plus 的 save 方法保存日志
    ;

    List<SystemLogs> getAllSystemLogs();

    void logSystem(SystemLogs log);
}
