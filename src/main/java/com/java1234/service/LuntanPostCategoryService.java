package com.java1234.service;

import com.java1234.entity.LuntanPostCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 86182
* @description 针对表【luntan_post_category】的数据库操作Service
* @createDate 2024-11-22 22:34:50
*/
public interface LuntanPostCategoryService extends IService<LuntanPostCategory> {

}
