package com.java1234.service;

import com.java1234.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 针对表【sys_user】的数据库操作Service
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 注册用户
     *
     * @param user 用户信息
     */
    void registerUser(SysUser user);

    /**
     * 根据用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户对象，如果找不到则返回 null
     */
    SysUser getByUsername(String username);

    /**
     * 获取用户的权限信息（角色和菜单权限）
     *
     * @param userId 用户ID
     * @return 权限信息字符串
     */
    String getUserAuthorityInfo(Long userId);

    /**
     * 检查用户名是否存在。
     *
     * @param username 用户名
     * @return 如果存在返回 true，否则返回 false
     */
    boolean checkUsernameExists(String username);

    /**
     * 检查手机号是否存在。
     *
     * @param phonenumber 手机号
     * @return 如果存在返回 true，否则返回 false
     */
    boolean checkPhonenumberExists(String phonenumber);

    /**
     * 重置用户密码。
     *
     * @param username   用户名
     * @param phonenumber 手机号
     * @param newPassword 新密码
     * @return 如果成功返回 true，否则返回 false
     */
    boolean resetPassword(String username, String phonenumber, String newPassword);
}