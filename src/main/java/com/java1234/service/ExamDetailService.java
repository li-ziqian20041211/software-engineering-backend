package com.java1234.service;

import com.java1234.entity.ExamDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java1234.entity.ExamQuestion;

import java.util.List;
import java.util.Map;

/**
* @author 86195
* @description 针对表【exam_detail(答题详情)】的数据库操作Service
* @createDate 2024-12-05 14:14:03
*/
public interface ExamDetailService extends IService<ExamDetail> {
    void saveExamDetails(Long examHistoryId, Map<String, Object> userAnswers, List<ExamQuestion> examQuestions);
    List<ExamDetail> getExamDetailsByExamHistoryId(Long examHistoryId);
}
