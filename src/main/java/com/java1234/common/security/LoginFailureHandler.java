package com.java1234.common.security;

import cn.hutool.json.JSONUtil;
import com.java1234.entity.LoginLog;
import com.java1234.entity.R;
import com.java1234.service.LoginLogService;
import com.java1234.util.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * 登录失败处理器
 */
@Slf4j
@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {

    @Autowired
    private LoginLogService loginLogService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();

        String message = e.getMessage();
        if (e instanceof BadCredentialsException) {
            message = "用户名或者密码错误！";
        }

        // 获取用户名
        String username = httpServletRequest.getParameter("username");

        // 记录登录失败日志
        recordLoginFailureLog(httpServletRequest, username, message);

        outputStream.write(JSONUtil.toJsonStr(R.error(message)).getBytes("UTF-8"));
        outputStream.flush();
        outputStream.close();
    }

    private void recordLoginFailureLog(HttpServletRequest request, String username, String message) {
        LoginLog loginLog = new LoginLog();
        loginLog.setUsername(username);
        loginLog.setIpAddress(IpUtil.getIpAddr(request));
        loginLog.setLoginTime(new Date());
        loginLog.setStatus("FAILURE");
        loginLog.setRemark(message);
        loginLog.setCreateTime(new Date());
        loginLog.setUpdateTime(new Date());

        loginLogService.logLogin(loginLog);
        log.info("用户 {} 登录失败，IP地址：{}，原因：{}", username, loginLog.getIpAddress(), message);
    }
}