package com.java1234.common.exception;

import com.java1234.entity.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

/**
 * 全部异常处理
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = RuntimeException.class)
    public R handler(RuntimeException e){
        log.error("运行时异常：---------{}"+e.getMessage());
        return R.error(e.getMessage());
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public R handleDuplicateKeyException(DuplicateKeyException e){
        log.error("数据库已存在该记录，请勿重复添加",e);
        return R.error("数据库已存在该记录，请勿重复添加");
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public  R handleMaxUploadSizeExceededException(){
        return R.error("上传文件大小超过20MB限制");
    }


}
