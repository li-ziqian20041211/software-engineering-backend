package com.java1234.common.exception;

/**
 * @description:
 * @author 邓清文
 * @since 2024/12/17 10:08
 * @version 1.0
 */
public class MyRunTimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MyRunTimeException() {}
    public MyRunTimeException(String message) {
        super(message);
    }
    public MyRunTimeException(String message, Throwable cause) {
        super(message, cause);
    }
    public MyRunTimeException(Throwable cause) {
        super(cause);
    }
}
