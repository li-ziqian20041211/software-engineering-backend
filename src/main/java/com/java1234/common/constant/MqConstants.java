package com.java1234.common.constant;

public class MqConstants {
    // 交换机名称
    public static final String ARTICLE_EXCHANGE = "article.topic";

    // 插入队列名称
    public static final String ARTICLE_INSERT_QUEUE = "article.insert.queue";

    // 删除队列名称
    public static final String ARTICLE_DELETE_QUEUE = "article.delete.queue";

    // 插入路由键
    public static final String ARTICLE_INSERT_KEY = "article.insert.key";

    // 删除路由键
    public static final String ARTICLE_DELETE_KEY = "article.delete.key";
}