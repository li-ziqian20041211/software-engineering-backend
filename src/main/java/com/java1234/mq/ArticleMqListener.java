package com.java1234.mq;

import com.java1234.common.constant.MqConstants;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class ArticleMqListener {

    @RabbitListener(queues = MqConstants.ARTICLE_INSERT_QUEUE)
    public void handleInsert(Integer id) {
        System.out.println("接收到新增文章的消息，ID: " + id);
        // 处理新增逻辑
    }

    @RabbitListener(queues = MqConstants.ARTICLE_DELETE_QUEUE)
    public void handleDelete(Integer id) {
        System.out.println("接收到删除文章的消息，ID: " + id);
        // 处理删除逻辑
    }
}