package com.java1234.controller;


import com.alibaba.fastjson.JSON;
import com.java1234.entity.*;
import com.java1234.service.ArticleService;
import com.java1234.service.ArticleTableService;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/es")
public class EsController {


    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private ArticleTableService articleTableService;

    /*
    初始化添加添加数据
     */
//    @PostMapping("/bulk")
//    void testBulkRequest() throws IOException {
//        // 查询所有的酒店数据
//        List<ArticleTable> list = articleTableService.list();
//
//        // 1.准备Request
//        BulkRequest request = new BulkRequest();
//        // 2.准备参数
//        for (ArticleTable article : list) {
//            // 2.1.转为HotelDoc
//            ArticleDoc articleDoc = new ArticleDoc(article);
//            // 2.2.转json
//            String json = JSON.toJSONString(articleDoc);
//            // 2.3.添加请求
//            request.add(new IndexRequest("article").id(article.getId().toString()).source(json, XContentType.JSON));
//        }
//
//        // 3.发送请求
//        client.bulk(request, RequestOptions.DEFAULT);
//        System.out.println("批量添加成功");
//    }

    @PostMapping("/pageList")
    public PageResult search(@RequestBody RequestParams params) {
        return articleTableService.search(params);
    }
}
