package com.java1234.controller;

import com.java1234.entity.R;
import com.java1234.entity.SysUser;
import com.java1234.service.SysUserService;
import com.java1234.service.UserFollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserFollowController {

    @Autowired
    private UserFollowService userFollowService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 检查是否已关注
     */
    @GetMapping("/checkFollow/{followingId}")
    public R checkFollow(@PathVariable Long followingId) {
        Long followerId = getCurrentUserId(); // 获取当前登录用户的ID
        if (followerId == null || followingId == null) {
            return R.error("用户ID不能为空");
        }
        boolean isFollowing = userFollowService.isFollowing(followerId, followingId);
        return R.ok().put("isFollowing", isFollowing);
    }

    /**
     * 关注作者
     */
    @PostMapping("/follow/{followingId}")
    public R followUser(@PathVariable Long followingId) {
        Long followerId = getCurrentUserId(); // 获取当前登录用户的ID
        if (followerId == null || followingId == null) {
            return R.error("用户ID不能为空");
        }
        if (followerId.equals(followingId)) {
            return R.error("不能关注自己");
        }
        try {
            userFollowService.followUser(followerId, followingId);
            return R.ok("关注成功");
        } catch (Exception e) {
            return R.error("关注失败：" + e.getMessage());
        }
    }

    /**
     * 取消关注作者
     */
    @PostMapping("/unfollow/{followingId}")
    public R unfollowUser(@PathVariable Long followingId) {
        Long followerId = getCurrentUserId(); // 获取当前登录用户的ID
        if (followerId == null || followingId == null) {
            return R.error("用户ID不能为空");
        }
        try {
            userFollowService.unfollowUser(followerId, followingId);
            return R.ok("取消关注成功");
        } catch (Exception e) {
            return R.error("取消关注失败：" + e.getMessage());
        }
    }

    /**
     * 获取当前登录用户的ID
     */
    private Long getCurrentUserId() {
        String username = getCurrentUsername(); // 获取当前登录用户名
        SysUser sysUser = sysUserService.getByUsername(username); // 根据用户名查询用户信息
        if (sysUser != null) {
            return sysUser.getId(); // 返回用户ID
        }
        return null;
    }
    private String getCurrentUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    /**
     * 获取用户的关注数量
     */
    @GetMapping("/followingCount/{userId}")
    public R getFollowingCount(@PathVariable Long userId) {
        if (userId == null) {
            return R.error("用户ID不能为空");
        }
        int followingCount = userFollowService.getFollowingCount(userId);
        return R.ok().put("followingCount", followingCount);
    }

    /**
     * 获取用户的粉丝数量
     */
    @GetMapping("/followerCount/{userId}")
    public R getFollowerCount(@PathVariable Long userId) {
        if (userId == null) {
            return R.error("用户ID不能为空");
        }
        int followerCount = userFollowService.getFollowerCount(userId);
        return R.ok().put("followerCount", followerCount);
    }
}
