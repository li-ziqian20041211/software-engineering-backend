package com.java1234.controller;

import com.github.pagehelper.PageInfo;
import com.java1234.entity.Operationlogs;
import com.java1234.entity.R;
import com.java1234.service.CollegeService;
import com.java1234.service.OperationLogsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import com.java1234.entity.College;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 学院Controller
 */
@RestController
@RequestMapping("/bsns/gradInfor/colleges")
public class CollegeController {
    private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);
    @Autowired
    private OperationLogsService operationlogsService; // 注入操作日志服务
    @Resource
    private CollegeService collegeService;
    /*
     * 删除学院
     * @param id
     */
    @PostMapping("/deleteById/{id}")
    public R deleteById(@PathVariable("id") Integer id,HttpServletRequest request){
        logger.info("删除院校信息: {}", id);
        collegeService.deleteById(id);
        logOperation(request, "删除院校", "POST /bsns/gradInfor/colleges/deleteById/{id}", "文章ID: " + id, "成功");
        return R.ok();
    }
    /*
     * 批量删除学院
     * @param ids
     */
    @PostMapping("/deleteBatch")
    public R deleteBatch(@RequestBody List<Integer> ids,HttpServletRequest request){
        logger.info("批量删除院校信息: {}", ids);
        collegeService.deleteBatch(ids);
        logOperation(request, "批量删除院校", "POST /bsns/gradInfor/colleges/deleteBatch", "文章ID: " + ids, "成功");
        return R.ok();
    }
    /*
     * 修改学院信息
     * @param college
     */

    @PostMapping("/update")
    public R update(@RequestBody College college,HttpServletRequest request){
        logger.info("修改院校信息: {}", college);
        collegeService.update(college);
        logOperation(request, "修改院校", "POST /bsns/gradInfor/colleges/update", "文章ID: " + college.getId(), "成功");
        return R.ok();
    }
    /*
     * 新增学院信息
     * @param college
     */
    @PostMapping("/add")
    public R add(@RequestBody College college,HttpServletRequest request){
        logger.info("新增院校信息: {}", college);
        collegeService.add(college);
        logOperation(request, "新增院校", "POST /bsns/gradInfor/colleges/add", "文章ID: " + college.getId(), "成功");
        return R.ok();
    }
    /*
     * 查询所有学院信息
     * @param college
     */
    @GetMapping("/selectAll")
    public R selectAll(College college,HttpServletRequest request){
        logger.info("查询所有院校信息: {}", college);
        Map<String,Object> resultMap=new HashMap<>();
        List<College> collegeList= collegeService.selectAll(college);
        resultMap.put("collegeList",collegeList);
        logOperation(request, "查询所有院校", "GET /bsns/gradInfor/colleges/selectAll", "文章ID: " + college.getId(), "成功");
        return R.ok(resultMap);//信息封装在resultMap中返回
    }
    /*
     * 根据id查询学院信息
     * @param id
     */
    @GetMapping("/selectById/{id}")
    public R selectById(@PathVariable("id") Integer id,HttpServletRequest request){
        logger.info("查询院校信息: {}", id);
        College college=collegeService.selectById(id);
        logOperation(request, "查询院校", "GET /bsns/gradInfor/colleges/selectById/{id}", "文章ID: " + id, "成功");
        return R.ok(college);
    }
    /*
     * 分页查询学院信息
     * @param college
     * @param pageNum
     * @param pageSize
     */
    //分页查询
    //pageNum当前页,pageSize每个页的个数
    @GetMapping("/selectPage")
    public R selectPage(College college,
                        @RequestParam(defaultValue = "1") Integer pageNum,
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        HttpServletRequest request){
        logger.info("分页查询院校信息: {}, {}, {}", college, pageNum, pageSize);
        PageInfo<College> pageInfo=collegeService.selectPage(college,pageNum,pageSize);
        logOperation(request, "分页查询院校", "GET /bsns/gradInfor/colleges/selectPage", "文章ID: " + college.getId(), "成功");
        return R.ok(pageInfo);
    }

    private void logOperation(HttpServletRequest request, String operation, String method, String params, String result) {
        Operationlogs log = new Operationlogs();
        log.setUsername(getCurrentUsername());
        log.setOperation(operation);
        log.setMethod(method);
        log.setParams(params);
        log.setResult(result);
        log.setElapsedTime(0L); // 可以计算实际耗时
        log.setCreateTime(new Date());
        log.setIpAddress(request.getRemoteAddr());
        log.setStatus("OK");
        operationlogsService.logOperation(log);
    }
    private String getCurrentUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            return principal.toString();
        }
    }


}
