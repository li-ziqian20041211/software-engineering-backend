package com.java1234.controller;

import com.java1234.entity.Message;
import com.java1234.service.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/17 10:48
 */
@RestController
@RequestMapping("/websocket")
public class WebSocketController {

    @Autowired
    private WebSocketServer webSocketServer;

    @PostMapping("/message")
    public String sendMessage(@RequestBody Message message) {
        Integer sendType=message.getSendType();
        switch (sendType) {
            case 1:
                webSocketServer.sendMessageToUser(message.getContent(), message.getUserid());
                break;
            case 2:
                webSocketServer.sendMessage(message.getContent());
                break;
            case 3:
                webSocketServer.sendMessage(message.getContent(), message.getUserid());
            default:
                break;
        }
        return "success";
    }
}
