package com.java1234.controller;

import com.java1234.entity.SystemLogs;
import com.java1234.service.SystemLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统日志Controller控制器
 */
@RestController
@RequestMapping("/systemLogs")
public class SystemLogsController {

    @Autowired
    private SystemLogsService systemLogsService;

    /**
     * 记录系统日志
     *
     * @param log 系统日志对象
     */
    @PostMapping
    public void logSystem(@RequestBody SystemLogs log) {
        systemLogsService.logSystem(log);
    }

    /**
     * 获取所有系统日志
     *
     * @return 系统日志列表
     */
    @GetMapping
    public List<SystemLogs> getAllSystemLogs() {
        return systemLogsService.getAllSystemLogs();
    }
}
