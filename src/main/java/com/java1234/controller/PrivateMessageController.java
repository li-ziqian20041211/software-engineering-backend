package com.java1234.controller;

import com.java1234.entity.PrivateMessage;
import com.java1234.entity.R;
import com.java1234.service.PrivateMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/message")
public class PrivateMessageController {

    @Autowired
    private PrivateMessageService privateMessageService;

    @GetMapping("/private")
    public R getPrivateMessagesByParams(@RequestParam Long senderId, @RequestParam Long receiverId) {
        List<PrivateMessage> messages = privateMessageService.getPrivateMessagesByUsers(senderId, receiverId);
        return R.ok(messages);
    }

    @GetMapping("/{senderId}/{receiverId}")
    public List<PrivateMessage> getPrivateMessagesByPath(
            @PathVariable Long senderId,
            @PathVariable Long receiverId) {
        return privateMessageService.getPrivateMessagesByUsers(senderId, receiverId);
    }
}
