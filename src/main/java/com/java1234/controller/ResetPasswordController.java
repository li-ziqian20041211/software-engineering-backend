package com.java1234.controller;

import com.java1234.service.SysUserService;
import com.java1234.util.DecryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 重置密码控制器
 */
@RestController
@RequestMapping("/api/resetpassword")
public class ResetPasswordController {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private DecryptUtil decryptUtil;  // 解密工具

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;  // bcrypt编码器

    /**
     * 检查用户名是否存在
     */
    @PostMapping("/check-username-exists")
    public ResponseEntity<Map<String, Object>> checkUsernameExists(@RequestBody Map<String, String> body) {
        boolean exists = sysUserService.checkUsernameExists(body.get("username"));
        return ResponseEntity.ok(createResponse(true, "操作成功", exists));
    }

    /**
     * 检查手机号是否存在
     */
    @PostMapping("/check-phonenumber-exists")
    public ResponseEntity<Map<String, Object>> checkPhonenumberExists(@RequestBody Map<String, String> body) {
        boolean exists = sysUserService.checkPhonenumberExists(body.get("phonenumber"));
        return ResponseEntity.ok(createResponse(true, "操作成功", exists));
    }

    /**
     * 重置密码
     */
    @PostMapping("/reset-password")
    public ResponseEntity<Map<String, Object>> resetPassword(@RequestBody Map<String, String> body) {
        try {
            String username = body.get("username");
            String phonenumber = body.get("phonenumber");
            String encryptedNewPassword = body.get("newPassword");

            // 解密新密码
            String decryptedNewPassword = decryptUtil.decrypt(encryptedNewPassword);

            // 使用bcrypt对解密后的密码进行哈希处理
            String hashedNewPassword = bCryptPasswordEncoder.encode(decryptedNewPassword);

            // 调用服务层重置密码，传递哈希后的密码
            boolean success = sysUserService.resetPassword(username, phonenumber, hashedNewPassword);
            if (success) {
                return ResponseEntity.ok(createResponse(true, "密码重置成功"));
            } else {
                return ResponseEntity.badRequest().body(createResponse(false, "密码重置失败"));
            }
        } catch (Exception e) {
            // 日志记录异常
            e.printStackTrace();
            return ResponseEntity.status(500).body(createResponse(false, "服务器内部错误：" + e.getMessage()));
        }
    }

    /**
     * 创建响应体
     */
    private Map<String, Object> createResponse(boolean success, String message) {
        Map<String, Object> response = new HashMap<>();
        response.put("success", success);
        response.put("message", message);
        return response;
    }

    /**
     * 创建带有 exists 字段的响应体
     */
    private Map<String, Object> createResponse(boolean success, String message, boolean exists) {
        Map<String, Object> response = createResponse(success, message);
        response.put("exists", exists); // 添加 exists 字段
        return response;
    }
}