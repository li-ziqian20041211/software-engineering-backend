package com.java1234.controller;

import com.java1234.entity.LoginLog;
import com.java1234.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 登录日志Controller
 */
@RestController
@RequestMapping("/loginLogs")
public class LoginLogController {

    @Autowired
    private LoginLogService loginLogService;

    /**
     * 记录登录日志
     *
     * @param log 登录日志对象
     */
    @PostMapping
    public void logLogin(@RequestBody LoginLog log) {
        loginLogService.logLogin(log);
    }

    /**
     * 获取所有登录日志
     *
     * @return 登录日志列表
     */
    @GetMapping
    public List<LoginLog> getAllLoginLogs() {
        return loginLogService.getAllLoginLogs();
    }
}