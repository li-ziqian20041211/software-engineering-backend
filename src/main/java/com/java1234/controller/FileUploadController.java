package com.java1234.controller;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSException;
import com.java1234.entity.Result;
import com.java1234.util.AliOssUtils;
import com.java1234.config.AliOssProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传 Controller
 * @author 邓清文
 */
@Slf4j
@RestController
public class FileUploadController {

    @Autowired
    private AliOssProperties aliOssProperties;

    @Autowired
    private AliOssUtils aliOssUtils;

    /**
     * 处理文件上传请求
     *
     * @param file 上传的文件
     * @return 包含上传文件信息的结果对象
     */
    @PostMapping("/upload")
    public Result<Map<String, Object>> upload(MultipartFile file) {
        try {
            // 获取文件的原始名称
            String originalFilename = file.getOriginalFilename();

            // 构造唯一的文件名，避免冲突
            String timestamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
            String filename = null;
            if (originalFilename != null) {
                filename = originalFilename.substring(0, originalFilename.lastIndexOf(".")) + "_" + timestamp + originalFilename.substring(originalFilename.lastIndexOf("."));
            }

            InputStream inputStream = file.getInputStream();
            String url = aliOssUtils.uploadFile(filename, inputStream);

            log.info("文件上传完成,文件名: {}", filename);
            log.info("文件上传完成,文件原始名称: {}", originalFilename);
            log.info("文件上传完成,文件大小: {}", file.getSize());
            log.info("文件上传完成,文件类型: {}", file.getContentType());

            // 创建 Map 对象来存储文件信息
            Map<String, Object> response = new HashMap<>();
            response.put("url", url);
            response.put("originalFilename", originalFilename);
            response.put("filename", filename);
            response.put("size", file.getSize());
            response.put("contentType", file.getContentType());
            return Result.ok(response);
        } catch (IOException e) {
            log.error("文件上传失败 (IO 错误): {}", e.getMessage(), e);
            return Result.error("文件上传失败: IO 错误 - " + e.getMessage());
        } catch (OSSException e) {
            log.error("文件上传失败 (OSS 错误): {}", e.getMessage(), e);
            return Result.error("文件上传失败: OSS 错误 - " + e.getMessage());
        } catch (ClientException e) {
            log.error("文件上传失败 (客户端错误): {}", e.getMessage(), e);
            return Result.error("文件上传失败: 客户端错误 - " + e.getMessage());
        } catch (Exception e) {
            log.error("文件上传失败 (未知错误): {}", e.getMessage(), e);
            return Result.error("文件上传失败: 未知错误 - " + e.getMessage());
        }
    }
}
