package com.java1234.controller;

import com.java1234.entity.Operationlogs;
import com.java1234.service.OperationLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 操作日志Controller
 */
@RestController
@RequestMapping("/operationLogs")
public class OperationLogsController {

    @Autowired
    private OperationLogsService operationLogService;

    /**
     * 记录操作日志
     *
     * @param log 操作日志对象
     */
    @PostMapping
    public void logOperation(@RequestBody Operationlogs log) {
        operationLogService.logOperation(log);
    }

    /**
     * 获取所有操作日志
     *
     * @return 操作日志列表
     */
    @GetMapping
    public List<Operationlogs> getAllLogs() {
        return operationLogService.getAllLogs();
    }
}
