package com.java1234.controller;

//import com.java1234.entity.R;
//import com.java1234.entity.SysUser;
//import com.java1234.service.SysUserService;
//import com.java1234.util.JwtUtils;
//import com.java1234.util.StringUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * 测试
// */
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//    @Autowired
//    private SysUserService sysUserService;
//
//    @RequestMapping("/user/list")
//    // @PreAuthorize("hasRole('ROLE_admin2')")
//    @PreAuthorize("hasAuthority('system:user2:list')")
//    public R userList(@RequestHeader(required = false)String token){
//        if(StringUtil.isNotEmpty(token)){
//            Map<String,Object> resutlMap=new HashMap<>();
//            List<SysUser> userList = sysUserService.list();
//            resutlMap.put("userList",userList);
//            return R.ok(resutlMap);
//        }else{
//            return R.error(401,"没有权限访问");
//        }
//
//    }
//
//    @RequestMapping("/login")
//    public R login(){
//        String token = JwtUtils.genJwtToken("java1234");
//        return R.ok().put("token",token);
//    }
//}
import com.java1234.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private RedisUtil redisUtil;

    @GetMapping("/test")
    public String testRedis() {
        String key = "testKey";
        String value = "testValue";

        // 设置缓存
        redisUtil.set(key, value, 60); // 缓存1分钟

        // 获取缓存
        Object cachedValue = redisUtil.get(key);

        return cachedValue != null ? cachedValue.toString() : "Cache miss";
    }
}