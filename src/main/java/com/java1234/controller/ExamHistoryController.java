package com.java1234.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java1234.entity.ExamDetail;
import com.java1234.entity.ExamHistory;
import com.java1234.entity.R;
import com.java1234.service.ExamDetailService;
import com.java1234.service.ExamHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 考试历史Controller
 */
@RestController
@RequestMapping("/bsns/subject/examHistorys")
public class ExamHistoryController {

    @Autowired
    private ExamHistoryService examHistoryService;

    @Autowired
    private ExamDetailService examDetailService;
    /**
     * 分页查询考试历史
     * @param userId 用户ID
     * @param page 当前页
     * @param size 每页显示的数量
     * @return
     */
    @GetMapping("/getExamHistory/{userId}")
    public R getExamHistory(@PathVariable Long userId, @RequestParam int page, @RequestParam int size) {
        IPage<ExamHistory> examHistories = examHistoryService.listByUserId(userId, new Page<>(page, size));
        return R.ok().put("examHistories", examHistories.getRecords()).put("total", examHistories.getTotal());
    }
    /**
     * 获取考试历史详情
     * @param examHistoryId 考试历史ID
     * @return
     */
    @GetMapping("/getExamDetail/{examHistoryId}")
    public R getExamDetail(@PathVariable Long examHistoryId) {
        List<ExamDetail> examDetails = examDetailService.getExamDetailsByExamHistoryId(examHistoryId);
        return R.ok().put("examDetails", examDetails);
    }
}