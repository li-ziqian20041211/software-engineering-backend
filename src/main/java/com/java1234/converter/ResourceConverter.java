package com.java1234.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.converters.ReadConverterContext;
import com.alibaba.excel.converters.WriteConverterContext;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.java1234.enums.ResTypeEnum;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/20 10:41
 */

public class ResourceConverter implements Converter<Short> {

    /**
     * 将Java对象数据(资料类型)转为Excel单元格数据
     */
    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Short> context) throws Exception {
        String resTypeDesc = ResTypeEnum.getDescByCode(Integer.valueOf(context.getValue()));
        return new WriteCellData<>(resTypeDesc);
    }


        /**
         * 将Excel单元格数据转为Java对象数据(资料类型)
         *
         */
    @Override
    public Short convertToJavaData(ReadConverterContext<?> context) throws Exception {
        Integer code = ResTypeEnum.getCodeByDesc(context.getReadCellData().getStringValue());

        return Short.valueOf(String.valueOf(code));
    }

}
