package com.java1234.exception;

import com.java1234.entity.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/16 10:41
 */

@RestControllerAdvice
@Slf4j
public class MyExceptionHandler {
    /**
     * 处理SQLIntegrityConstraintViolationException异常
     * @param e/
     * @return R
     */
//    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
//     public R handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e){
//         return R.error(500,"数据库已存在该记录，请勿重复添加");
//     }


    @ExceptionHandler(DuplicateKeyException.class)
     public R handleDuplicateKeyException(DuplicateKeyException e){
         log.error("数据库已存在该记录，请勿重复添加",e);
         return R.error("数据库已存在该记录，请勿重复添加");
     }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public  R handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e){
        return R.error("上传文件大小超过20MB限制");
    }
    @ExceptionHandler(NoHandlerFoundException.class)
    public R handlerNoFoundException(Exception e) {
        return R.error(404,"路径不存在，请检查路径是否正确");
    }
//     @ExceptionHandler(RuntimeException.class)
//    public R runtimeException(RuntimeException e){
//        log.error("运行时异常",e);
//         return R.error(e.getMessage());
//     }


}
