package com.java1234.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisPlus配置类
 */
@Configuration
public class MybatisPlusConfig {
    @Bean
    //PaginationInterceptor是一个分页插件:在执行数据库查询时自动将查询结果进行分页处理
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }
}

