package com.java1234.config;



import com.java1234.common.constant.MqConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class MqConfig {

    // 定义交换机
    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(MqConstants.ARTICLE_EXCHANGE, true, false);
    }


    // 定义插入队列
    @Bean
    public Queue insertQueue() {
        return new Queue(MqConstants.ARTICLE_INSERT_QUEUE, true);
    }

    // 定义删除队列
    @Bean
    public Queue deleteQueue() {
        return new Queue(MqConstants.ARTICLE_DELETE_QUEUE, true);
    }

    // 绑定插入队列到交换机
    @Bean
    public Binding insertQueueBinding() {
        return BindingBuilder.bind(insertQueue()).to(topicExchange()).with(MqConstants.ARTICLE_INSERT_KEY);
    }

    // 绑定删除队列到交换机
    @Bean
    public Binding deleteQueueBinding() {
        return BindingBuilder.bind(deleteQueue()).to(topicExchange()).with(MqConstants.ARTICLE_DELETE_KEY);
    }
}