package com.java1234.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/17 9:18
 */
@Configuration
public class WebSocketConfig {
    /**
     * ServerEndpointExporter的主要作用是通过Spring配置来声明并注册 WebSocket端点。
     * 当Spring Boot应用启动时，ServerEndpointExporter会扫描所有带有 @ServerEndpoint注
     解的类，
     * 并将它们注册为WebSocket端点，从而允许客户端与之建立WebSocket连接。
     *
     * @return ServerEndpointExporter
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
