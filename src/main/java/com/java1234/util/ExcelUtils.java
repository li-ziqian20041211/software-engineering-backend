package com.java1234.util;



import com.java1234.common.exception.MyRunTimeException;
import com.alibaba.excel.EasyExcel;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/20 10:13
 */

public class ExcelUtils<T> {

    /**

     * 导出
     *

     * @param response   响应对象

     * @param exportList 带导出的列表

     * @param clazz      导出对象class

     * @param fileName   导出的文件名

     * @param sheetName  工作表名

     * @param <T>
     */
    public static <T> void exportExcel(HttpServletResponse response, List<T> exportList, Class<T> clazz, String fileName, String sheetName) {
        try {
            // 设置响应头信息
            setResponseHeader(response, fileName, sheetName);

            // 使用EasyExcel导出数据
            EasyExcel.write(response.getOutputStream(), clazz).sheet(sheetName).doWrite(exportList);

        } catch (IOException e) {
            throw new MyRunTimeException("Excel 导出失败");
        }
    }

    /**

     * 导入模版下载
     *

     * @param response  响应对象

     * @param clazz     导出对象class

     * @param fileName  导出的文件名

     * @param sheetName 工作表名
     */
    public static <T> void downloadTemplate(HttpServletResponse response, Class<T> clazz, String fileName, String sheetName) {
        try {
            // 设置响应头信息
            setResponseHeader(response, fileName, sheetName);

            EasyExcel.write(response.getOutputStream(), clazz).sheet(sheetName).doWrite(new ArrayList<>());

        } catch (IOException e) {
            throw new MyRunTimeException("Excel 导入模版下载失败");
        }
    }

    /**

     * 导入
     *

     * @param file  待导入的excel文件

     * @param clazz 导入对象

     * @param <T>

     * @return 待导入的数据列表
     */
    public static <T> List<T> importExcel(MultipartFile file, Class<T> clazz) {
        try {
            // 读取Excel中的数据
            return EasyExcel.read(file.getInputStream())
                    .head(clazz)
                    .sheet()
                    .doReadSync();

        } catch (IOException e) {
            // 抛出自定义异常
            throw new MyRunTimeException("Excel 导入失败");
        }
    }

    /**

     * 设置响应头信息
     *
     * @param response  响应对象
     * @param fileName  文件名
     * @param sheetName 工作表名
     * @param <T>
     */
    private static <T> void setResponseHeader(HttpServletResponse response, String fileName, String sheetName) throws UnsupportedEncodingException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String exportFileName = URLEncoder.encode(fileName, StandardCharsets.UTF_8).replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + exportFileName + ".xlsx");
    }

}
