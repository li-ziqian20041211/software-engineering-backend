//package com.java1234.util;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.java1234.entity.Answer;
//import com.java1234.entity.Question;
//import com.java1234.service.AnswerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Component
//public class ScoreCalculator {
//
//    @Autowired
//    private AnswerService answerService;
//
//    public double calculateScore(Question question, Object userAnswer) {
//        // 获取正确答案
//        List<Answer> correctAnswers = answerService.list(new QueryWrapper<Answer>()
//                .eq("question_id", question.getId())
//                .eq("is_answer", true));
//
//        // 添加日志打印
//        System.out.println("Question ID: " + question.getId());
//        System.out.println("User Answer: " + userAnswer);
//        System.out.println("Correct Answers: " + correctAnswers);
//
//        // 根据题型不同，计算每题得分
//        switch (question.getType()) {
//            case 1: // 单选题
//                // 单选题的逻辑，只需判断用户选择的答案是否在正确答案列表中
//                if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
//                    return Double.parseDouble(question.getScore());
//                }
//                break;
//            case 2: // 多选题
//                // 多选题的逻辑，需要判断用户选择的答案是否与所有正确答案一致
//                if (userAnswer != null && userAnswer instanceof List) {
//                    List<String> userSelectedAnswers = (List<String>) userAnswer;
//                    if (isCorrectAnswer(correctAnswers, userSelectedAnswers)) {
//                        return Double.parseDouble(question.getScore());
//                    }
//                }
//                break;
//            case 3: // 判断题
//                // 判断题的逻辑，需要根据正确答案的数量来判断是单选还是多选
//                if (correctAnswers.size() == 1) {
//                    // 判断题（单选）
//                    if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
//                        return Double.parseDouble(question.getScore());
//                    }
//                } else {
//                    // 判断题（多选）
//                    if (userAnswer != null && userAnswer instanceof List) {
//                        List<String> userSelectedAnswers = (List<String>) userAnswer;
//                        if (isCorrectAnswer(correctAnswers, userSelectedAnswers)) {
//                            return Double.parseDouble(question.getScore());
//                        }
//                    }
//                }
//                break;
//            case 4: // 填空题
//                // 填空题的逻辑，需要判断用户填写的答案是否在正确答案列表中
//                if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
//                    return Double.parseDouble(question.getScore());
//                }
//                break;
//            default:
//                return 0;
//        }
//        // 返回该题得分
//        return 0;
//    }
//
//    private boolean isCorrectAnswer(List<Answer> correctAnswers, String userAnswer) {
//        return correctAnswers.stream()
//                .map(Answer::getAnswerContent)
//                .anyMatch(correctAnswer -> correctAnswer.equals(userAnswer));
//    }
//
//    private boolean isCorrectAnswer(List<Answer> correctAnswers, List<String> userAnswers) {
//        List<String> correctAnswerContents = correctAnswers.stream()
//                .map(Answer::getAnswerContent)
//                .collect(Collectors.toList());
//        return userAnswers.size() == correctAnswerContents.size() &&
//                userAnswers.containsAll(correctAnswerContents) &&
//                correctAnswerContents.containsAll(userAnswers);
//    }
//}

package com.java1234.util;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.java1234.entity.Answer;
import com.java1234.entity.Question;
import com.java1234.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ScoreCalculator {

    @Autowired
    private AnswerService answerService;

        public double calculateScore(Question question, Object userAnswer) {
        // 获取正确答案
        List<Answer> correctAnswers = answerService.list(new QueryWrapper<Answer>()
                .eq("question_id", question.getId())
                .eq("is_answer", true));

        // 添加日志打印
        System.out.println("Question ID: " + question.getId());
        System.out.println("User Answer: " + userAnswer);
        System.out.println("Correct Answers: " + correctAnswers);

        // 根据题型不同，计算每题得分
        switch (question.getType()) {
            case 1: // 单选题
                // 单选题的逻辑，只需判断用户选择的答案是否在正确答案列表中
                if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
                    return Double.parseDouble(question.getScore());
                }
                break;
            case 2: // 多选题
                // 多选题的逻辑，需要判断用户选择的答案是否与所有正确答案一致
                if (userAnswer != null && userAnswer instanceof List) {
                    List<String> userSelectedAnswers = (List<String>) userAnswer;
                    if (isCorrectAnswer(correctAnswers, userSelectedAnswers)) {
                        return Double.parseDouble(question.getScore());
                    }
                }
                break;
            case 3: // 判断题
                // 判断题的逻辑，需要根据正确答案的数量来判断是单选还是多选
                if (correctAnswers.size() == 1) {
                    // 判断题（单选）
                    if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
                        return Double.parseDouble(question.getScore());
                    }
                } else {
                    // 判断题（多选）
                    if (userAnswer != null && userAnswer instanceof List) {
                        List<String> userSelectedAnswers = (List<String>) userAnswer;
                        if (isCorrectAnswer(correctAnswers, userSelectedAnswers)) {
                            return Double.parseDouble(question.getScore());
                        }
                    }
                }
                break;
            case 4: // 填空题
                // 填空题的逻辑，需要判断用户填写的答案是否在正确答案列表中
                if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
                    return Double.parseDouble(question.getScore());
                }
                break;
            default:
                return 0;
        }
        // 返回该题得分
        return 0;
    }

//    public double calculateScore(Question question, Object userAnswer, List<Answer> correctAnswers) {
//        // 添加日志打印
//        System.out.println("Question ID: " + question.getId());
//        System.out.println("User Answer: " + userAnswer);
//        System.out.println("Correct Answers: " + correctAnswers);
//
//        // 根据题型不同，计算每题得分
//        switch (question.getType()) {
//            case 1: // 单选题
//                // 单选题的逻辑，只需判断用户选择的答案是否在正确答案列表中
//                if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
//                    return Double.parseDouble(question.getScore());
//                }
//                break;
//            case 2: // 多选题
//                // 多选题的逻辑，需要判断用户选择的答案是否与所有正确答案一致
//                if (userAnswer != null && userAnswer instanceof List) {
//                    List<String> userSelectedAnswers = (List<String>) userAnswer;
//                    if (isCorrectAnswer(correctAnswers, userSelectedAnswers)) {
//                        return Double.parseDouble(question.getScore());
//                    }
//                }
//                break;
//            case 3: // 判断题
//                // 判断题的逻辑，需要根据正确答案的数量来判断是单选还是多选
//                if (correctAnswers.size() == 1) {
//                    // 判断题（单选）
//                    if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
//                        return Double.parseDouble(question.getScore());
//                    }
//                } else {
//                    // 判断题（多选）
//                    if (userAnswer != null && userAnswer instanceof List) {
//                        List<String> userSelectedAnswers = (List<String>) userAnswer;
//                        if (isCorrectAnswer(correctAnswers, userSelectedAnswers)) {
//                            return Double.parseDouble(question.getScore());
//                        }
//                    }
//                }
//                break;
//            case 4: // 填空题
//                // 填空题的逻辑，需要判断用户填写的答案是否在正确答案列表中
//                if (userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer)) {
//                    return Double.parseDouble(question.getScore());
//                }
//                break;
//            default:
//                return 0;
//        }
//        // 返回该题得分
//        return 0;
//    }

    public boolean isCorrect(Question question, Object userAnswer, List<Answer> correctAnswers) {
        switch (question.getType()) {
            case 1: // 单选题
                return userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer);
            case 2: // 多选题
                if (userAnswer != null && userAnswer instanceof List) {
                    List<String> userSelectedAnswers = (List<String>) userAnswer;
                    return isCorrectAnswer(correctAnswers, userSelectedAnswers);
                }
                break;
            case 3: // 判断题
                if (correctAnswers.size() == 1) {
                    return userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer);
                } else {
                    if (userAnswer != null && userAnswer instanceof List) {
                        List<String> userSelectedAnswers = (List<String>) userAnswer;
                        return isCorrectAnswer(correctAnswers, userSelectedAnswers);
                    }
                }
                break;
            case 4: // 填空题
                return userAnswer != null && userAnswer instanceof String && isCorrectAnswer(correctAnswers, (String) userAnswer);
            default:
                return false;
        }
        return false;
    }

    private boolean isCorrectAnswer(List<Answer> correctAnswers, String userAnswer) {
        return correctAnswers.stream()
                .map(Answer::getAnswerContent)
                .anyMatch(correctAnswer -> correctAnswer.equals(userAnswer));
    }

    private boolean isCorrectAnswer(List<Answer> correctAnswers, List<String> userAnswers) {
        List<String> correctAnswerContents = correctAnswers.stream()
                .map(Answer::getAnswerContent)
                .collect(Collectors.toList());
        return userAnswers.size() == correctAnswerContents.size() &&
                userAnswers.containsAll(correctAnswerContents) &&
                correctAnswerContents.containsAll(userAnswers);
    }
}