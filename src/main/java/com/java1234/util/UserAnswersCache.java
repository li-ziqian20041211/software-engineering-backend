package com.java1234.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserAnswersCache {
    private static final Map<Long, Map<String, Object>> userAnswersMap = new ConcurrentHashMap<>();

    public static void saveUserAnswers(Long examId, Map<String, Object> userAnswers) {
        userAnswersMap.put(examId, userAnswers);
    }

    public static Map<String, Object> getUserAnswers(Long examId) {
        return userAnswersMap.get(examId);
    }
}