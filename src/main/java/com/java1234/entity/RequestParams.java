package com.java1234.entity;

import lombok.Data;

import java.util.List;

@Data
public class RequestParams {
    private  Integer id;
    private String title;
    private  String img;
    private  String descriptionUrl;
    private  String time;
    private  String articleType;
    private  String content;
    private List<String> articleTypes;
    private List<String> times;

    private String key;
    private Integer page;
    private Integer size;


}
