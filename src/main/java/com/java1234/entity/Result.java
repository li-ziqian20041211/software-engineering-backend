package com.java1234.entity;

import lombok.Data;

import java.io.Serializable;

/**

 * @author 邓清文
 * @version 1.0
 * @since 2024/12/16 9:03
 */
@Data
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**

     * 成功状态码
     */
    public static final int SUCCESS_CODE = 200;
    /**
     * 失败状态码
     */
    public static final int ERROR_CODE = 500;

    /**

     * 请求状态 true:成功，false:失败
     */
    private Boolean success;

    /**

     * 请求状态码，200 500 404
     */
    private Integer code;

    /**

     * 返回的消息内容
     */
    private String message;

    /**

     * 返回的数据
     */
    private T data;

    /**

     * 时间戳
     */
    private Long timestamp = System.currentTimeMillis();

    public Result() {
    }


    /**
     * 操作成功
     *
     */
    public static <T> Result<T> ok() {
        Result<T> r = new Result<>();
        r.setSuccess(Boolean.TRUE);
        r.setCode(SUCCESS_CODE);
        r.setMessage("操作成功！");
        return r;
    }

    public static <T> Result<T> ok(String message) {
        Result<T> r = ok();
        r.setMessage(message);
        return r;
    }

    public static <T> Result<T> ok(T data) {
        Result<T> r = ok();
        r.setData(data);
        return r;
    }

    public static <T> Result<T> ok(T data, String message) {
        Result<T> r = ok();
        r.setData(data);
        r.setMessage(message);
        return r;
    }

    /**
     * 操作失败
     *
     */
    public static <T> Result<T> error() {
        Result<T> r = new Result<>();
        r.setSuccess(Boolean.FALSE);
        r.setCode(ERROR_CODE);
        r.setMessage("操作失败！");
        return r;
    }

    public static <T> Result<T> error(String message) {
        Result<T> r = error();
        r.setMessage(message);
        return r;
    }

    public static <T> Result<T> error(int code) {
        Result<T> r = error();
        r.setCode(code);
        return r;
    }

    public static <T> Result<T> error(String message, int code) {
        Result<T> r = error();
        r.setMessage(message);
        r.setCode(code);
        return r;
    }

}