package com.java1234.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class College {
    private Integer id;
    private String name;
    private String img;
    private String description;
    private String region;
    private String collegeLevel;

    private List<String> regions;
    private List<String> college_levels;
}
