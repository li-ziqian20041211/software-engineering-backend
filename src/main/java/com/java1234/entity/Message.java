package com.java1234.entity;

import lombok.Data;

/**
 * @author 邓清文
 * @version 1.0
 * @description:
 * @since 2024/12/17 10:50
 */
@Data
public class Message {
    /**
     * 1-给指定用户
     * 2-给所有在线用户（包括自己）
     * 3-给所有用户（不包括自己）
     */
    private int sendType;

    private String userid;

    private String content;
}
