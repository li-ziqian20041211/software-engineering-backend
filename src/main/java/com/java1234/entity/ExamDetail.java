package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 答题详情
 * @TableName exam_detail
 */
@TableName(value ="exam_detail")
@Data
public class ExamDetail implements Serializable {
    /**
     * 答题详情id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 答题记录id
     */
    @TableField(value = "exam_history_id")
    private Long examHistoryId;

    /**
     * 关联题目
     */
    @TableField(value = "question_id")
    private Integer questionId;

    /**
     * 用户答案
     */
    @TableField(value = "user_answer")
    private String userAnswer;

    /**
     * 正确答案
     */
    @TableField(value = "correct_answer")
    private String correctAnswer;

    /**
     * 是否正确
     */
    @TableField(value = "is_correct")
    private Boolean  isCorrect;

    /**
     * 该题分数
     */
    @TableField(value = "score")
    private Double score;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}