package com.java1234.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.java1234.converter.ResourceConverter;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 资源实体类
 * @TableName resource
 * @author 邓清文
 */
@TableName(value = "resource")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Emp implements Serializable {
    /**
     * ID
     */
    @ExcelIgnore
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 资源名
     */
    @ColumnWidth(35)
    @ExcelProperty(value = "资料名")
    @TableField(value = "name")
    private String name;

    /**
     * 资源类型
     */
    @ColumnWidth(18)
    @ExcelProperty(value = "资源类型", converter = ResourceConverter.class)
    @TableField(value = "restype")
    private Short restype;

    @ColumnWidth(6)
    @ExcelProperty("下载次数")
    @TableField(value = "cont")
    private Integer cont;

    /**
     * 图像URL
     */
    @ColumnWidth(60)
    @ExcelProperty("图像URL")
    @TableField(value = "image")
    private String image;

    /**
     * 上传日期
     */
    @ColumnWidth(22)
    @ExcelProperty("上传日期")
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @ColumnWidth(22)
    @ExcelProperty("修改时间")
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 序列化版本UID
     */
    private static final long serialVersionUID = 1L;
}