package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName private_message
 */
@TableName(value ="private_message")
@Data
public class PrivateMessage implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    @TableField(value = "sender_id")
    private Long senderId;

    /**
     * 
     */
    @TableField(value = "receiver_id")
    private Long receiverId;

    /**
     * 
     */
    @TableField(value = "content")
    private String content;

    /**
     * 
     */
    @TableField(value = "sent_at")
    private LocalDateTime sentAt;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}