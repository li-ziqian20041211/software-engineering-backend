package com.java1234.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author 张若瑜
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleDoc {
    private  Integer id;
    private String title;
    private  String img;
    private  String descriptionUrl;
    private  String time;
    private  String articleType;


    public ArticleDoc(ArticleTable article) {
        this.id = article.getId();
        this.title = article.getTitle();
        this.img = article.getImg();
        this.descriptionUrl = article.getDescriptionUrl();
        this.time = article.getTime();
        this.articleType = article.getArticleType();

    }

}
