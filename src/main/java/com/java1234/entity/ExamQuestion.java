package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 试卷-试题关联表
 * @TableName exam_question
 */
@TableName(value ="exam_question")
@Data
public class ExamQuestion implements Serializable {
    /**
     * 试卷id
     */
    @TableId(value = "examination_id")
    private Integer examinationId;

    /**
     * 试题id
     */
    @TableField(value = "question_id")
    private Integer questionId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}