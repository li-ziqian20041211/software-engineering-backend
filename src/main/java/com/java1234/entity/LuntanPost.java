package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 *
 * @TableName luntan_post
 */
@TableName(value ="luntan_post")
@Data
public class LuntanPost implements Serializable {

    public static final int Article_TOP = 1;

    public static final int Article_Common = 0;
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 标题
     */
    @TableField(value = "title")
    private String title;

    /**
     * 简介
     */
    @TableField(value = "summary")
    private String summary;

    /**
     * 内容
     */
    @TableField(value = "content")
    private String content;


    /**
     * 状态（0正常 1停用）
     */
    @TableField(value = "status")
    private String status;

    /**
     * 发布时间
     */
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**
     * 浏览量
     */
    @TableField(value = "views")
    private Integer views;

    /**
     * 点赞
     */
    @TableField(value = "likes")
    private Integer likes;

    /**
     * 评论
     */
    @TableField(value = "comments")
    private Integer comments;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 帖子封面
     */
    @TableField(value = "cover")
    private String cover;

    /**
     * 父级ID
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 是否为评论
     */
    @TableField(value = "is_comment")
    private Boolean isComment;

    /**
     * 是否为子评论
     */
    @TableField(value = "is_reply")
    private Boolean isReply;

    /**
     * 子评论列表
     */
    @TableField(exist = false)
    private List<LuntanPost> replies;

    /**
     * 是否为草稿
     */
    @TableField(value = "is_draft")
    private Integer isDraft;

    private Long publisherId; // 发布者 ID

    @TableField(exist = false)
    private List<Integer> categoryIds;

    public List<Integer> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Integer> categoryIds) {
        this.categoryIds = categoryIds;
    }
    // 新增用于直接存储分类名称的属性，方便在展示等场景直接使用
    @TableField(exist = false)
    private List<String> categoryNames; // 分类名称列表

    // 新增用于直接存储发布人姓名的属性，方便在展示等场景直接使用
    @TableField(value = "publisherName")
    private String publisherName;

    // Getter和Setter方法，虽然使用@Data注解会自动生成，但为了清晰展示列出关键的
    public List<String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(List<String> categoryNames) {
        this.categoryNames = categoryNames;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public List<LuntanPost> getReplies() {
        return replies;
    }

    public void setReplies(List<LuntanPost> replies) {
        this.replies = replies;
    }

    public Integer getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Long  getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

}