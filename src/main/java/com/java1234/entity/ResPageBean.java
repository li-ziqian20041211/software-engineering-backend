package com.java1234.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页查询结果封装类
 * @author 邓清文
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResPageBean {

    private Long total;
    private List rows;

}
