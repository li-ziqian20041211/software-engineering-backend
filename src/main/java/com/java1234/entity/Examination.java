package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 试卷表
 * @TableName examination
 */
@TableName(value ="examination")
@Data
public class Examination implements Serializable {
    /**
     * 试卷id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 试卷名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 组卷类型（1:人工组卷；2:抽题组卷；3:随机组卷）
     */
    @TableField(value = "type")
    private Integer type;

    /**
     * 试卷总分
     */
    @TableField(value = "score")
    private Integer score;

    /**
     * 试题数量
     */
    @TableField(value = "examNum")
    private Integer examnum;

    /**
     * 考试时长(单位：分钟)
     */
    @TableField(value = "duration")
    private Integer duration;

    /**
     * 试卷状态（1:已发布；2:待发布；3:撤回）
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 试卷难度（1到5）
     */
    @TableField(value = "difficultylevel")
    private Integer difficultylevel;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}