package com.java1234.entity;

import lombok.Data;

import java.util.List;

// 分页实体类
@Data
public class PageBean {
    private int pageNum; // 第几页
    private int pageSize; // 每页记录数
    private int start;  // 起始页
    private String query; // 查询参数
    private Integer questionType;
    private Long questionBankId;
    private Integer status;
    private String publisher;
    private String category;
    private String difficultylevel;
    private String content; // 评论内容
    private String postTitle; // 帖子标题




    // 新增字段
    private Long total;
    private List<?> rows;



    public PageBean() {
    }

    public PageBean(Long total, List<?> rows) {

        this.total = total;
        this.rows = rows;
    }

    // 获取帖子标题
    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    // 获取评论内容
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }



    public PageBean(int pageNum, int pageSize, String query) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.query = query;
    }

    public PageBean(int pageNum, int pageSize, String query, Integer questionType, Long questionBankId) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.query = query;
        this.questionType = questionType;
        this.questionBankId = questionBankId;
    }

    public PageBean(int pageNum, int pageSize) {
        super();
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public PageBean(int pageNum, int pageSize, String query, String publisher, String category) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.query = query;
        this.publisher = publisher;
        this.category = category;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getStart() {
        return (pageNum - 1) * pageSize;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public Long getQuestionBankId() {
        return questionBankId;
    }

    public void setQuestionBankId(Long questionBankId) {
        this.questionBankId = questionBankId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    // 获取分类名称
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    // 获取发布人名称
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    // 获取难度级别
    public String getDifficultylevel() {
        return difficultylevel;
    }

    public void setDifficultylevel(String difficultylevel) {
        this.difficultylevel = difficultylevel;
    }

    // 新增getter和setter方法
    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }
}
