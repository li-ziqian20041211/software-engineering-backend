package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName luntan_post_category
 */
@TableName(value ="luntan_post_category")
@Data
public class LuntanPostCategory implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    @TableField(value = "post_id")
    private Integer postId;

    /**
     * 
     */
    @TableField(value = "category_id")
    private Integer categoryId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    // 添加接受两个参数的构造函数
    public LuntanPostCategory(Integer postId, Integer categoryId) {
        this.postId = postId;
        this.categoryId = categoryId;
    }
}