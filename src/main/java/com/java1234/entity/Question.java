package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 试题表
 * @TableName question
 */
@TableName(value ="question")
@Data
public class Question implements Serializable {
    /**
     * 试题id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 所属题库id
     */
    @TableField(value = "bankid")
    private Integer bankid;

    /**
     * 试题类型(1:单选；2:多选；3:判断；4:填空)
     */
    @TableField(value = "type")
    private Integer type;

    /**
     * 题目
     */
    @NotEmpty(message = "题目不能为空")
    @TableField(value = "title")
    private String title;

    /**
     * 难度等级(1到5)
     */
    @TableField(value = "difficultylevel")
    private Integer difficultylevel;

    /**
     * 题目解析
     */
    @TableField(value = "analysis")
    private String analysis;

    /*
    * 题目分数
    */
    @TableField(value = "score")
    private String score;


    /**
     * 题目图片路径
     */
    @TableField(value = "imageUrl")
    private String imageURL;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }
}