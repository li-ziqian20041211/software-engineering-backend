package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName past_paper
 */
@TableName(value ="past_paper")
@Data
public class PastPaper implements Serializable {
    /**
     * 真题id
     */
    @TableId(value = "paper_id", type = IdType.AUTO)
    private Integer paperId;

    /**
     * 地区名称
     */
    @TableField(value = "region")
    private String region;

    /**
     * 学校名称
     */
    @TableField(value = "school")
    private String school;

    /**
     * 真题年份
     */
    @TableField(value = "year")
    private Object year;

    /**
     * 真题难度等级
     */
    @TableField(value = "difficulty")
    private Integer difficulty;

    /**
     * 下载量
     */
    @TableField(value = "downloads")
    private Integer downloads;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}