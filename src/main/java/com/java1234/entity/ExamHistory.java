package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 练习记录
 * @TableName exam_history
 */
@TableName(value ="exam_history")
@Data
public class ExamHistory implements Serializable {
    /**
     * 练习记录id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 试卷id
     */
    @TableField(value = "exam_id")
    private Long examId;

    /**
     * 练习总分数
     */
    @TableField(value = "total_score")
    private Double totalScore;

    /**
     * 练习时间
     */
    @TableField(value = "exam_time")
    private Date examTime;

    /**
     * 试卷名称
     */
    @TableField(value = "exam_name")
    private String examName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}