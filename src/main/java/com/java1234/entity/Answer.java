package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 答案表
 * @TableName answer
 */
@TableName(value ="answer")
@Data
public class Answer implements Serializable {
    /**
     * 答案id
     */
    @TableId(value = "id")
    private Integer id;

    /**
     * 是否是答案
     */
    @TableField(value = "is_answer")
    private Integer isAnswer;

    /**
     * 关联的试题id
     */
    @TableField(value = "question_id")
    private Integer questionId;

    /**
     * 选项图片的URL
     */
    @TableField(value = "option_image")
    private String optionImage;

    /**
     * 答案内容
     */
    @TableField(value = "answer_content")
    private String answerContent;

    /**
     * 答案解析
     */
    @TableField(value = "answer_analysis")
    private String answerAnalysis;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}