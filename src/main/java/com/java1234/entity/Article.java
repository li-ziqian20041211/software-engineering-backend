package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
//import jdk.jfr.Enabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * @author 张若瑜
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("article")
public class Article {

    private  Integer id;
    @TableField(value = "title")
    private String title;
    private  String img;

    private  String descriptionUrl;
    private  String time;

    private  String articleType;
    private  String content;
    private List<String> articleTypes;
    private List<String> times;

}
