package com.java1234.entity;

import lombok.Data;

import java.util.List;

@Data
public class PageResult {
    private Long total;
    private List<ArticleDoc> articles;

    public PageResult() {
    }

    public PageResult(Long total, List<ArticleDoc> articles) {
        this.total = total;
        this.articles = articles;
    }
}
