package com.java1234.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "article")
public class ArticleTable {
    private  Integer id;
    private String title;
    private  String img;
    @TableField(value = "descriptionUrl")
    private  String descriptionUrl;
    private  String time;
    @TableField(value = "articleType")
    private  String articleType;
    private  String content;
//    private List<String> articleTypes;
//    private List<String> times;

}
